/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __IIC_CHAR_LCD_H__
#define __IIC_CHAR_LCD_H__

//////////////////////////////////////////////////////
//
//   LCD INTERFACE TYPES
//
//////////////////////////////////////////////////////
#define LCD_8_BIT_BIG      0x01   // 8 bit interface, 2 Es
#define LCD_4_BIT_BIG      0x02   // 4 bit interface, 2 Es
#define LCD_8_BIT_SML      0x04   // 8 bit interface, 1 E
#define LCD_4_BIT_SML      0x08   // 4 bit interface, 1 E

//////////////////////////////////////////////////////
//
//   DATA AND CONTROL LINE DEFINES
//
//   Mask does not affect port state when mask bit is 1
//
//   PCF8574   LCD         D_M      C_M1   C_M2
//   ----------------------------------------
//   P7      (14)D7      0      1      1
//   P6      (13)D6      0      1      1
//   P5      (12)D5      0      1      1
//   P4      (11)D4      0      1      1
//   P3      (5) R/W     1      0      0
//   P2      (4) RS      1      0      0
//   P1      (6) E       1      0      1
//   P0      (?) E2      1      1      0
//
//   DATA_MASK           0x0F
//   CONTROL_MASK_1      0xF8
//   CONTROL_MASK_2      0xF4
//
//////////////////////////////////////////////////////
#define IIC_LCD_CTRL_PORT         IIC_ADDRESS_7
#define IIC_LCD_DATA_PORT         IIC_ADDRESS_7
#define IIC_LCD_DATA_MASK         0x0F
#define IIC_LCD_CONTROL_MASK_1    0xF1
#define IIC_LCD_CONTROL_MASK_2    0xF2

//bit numbers for _BV() avr-libc
#define LCD_BF                   7
#define LCD_R_W                  3
#define LCD_RW                   3
#define LCD_RS                   2
#define LCD_E                    1
#define LCD_E2                   0

#define LCD_DISPLAY_ONE          IIC_LCD_CONTROL_MASK_1
#define LCD_DISPLAY_TWO          IIC_LCD_CONTROL_MASK_2

#define LCD_LINE_ONE             0x80
#define LCD_LINE_TWO             0xC0
#define LCD_LINE_THREE           0x94
#define LCD_LINE_FOUR            0xD4
#define LCD_CHAR_WIDTH           5
#define LCD_CHAR_HEIGHT          7

//////////////////////////////////////////////////////
//
//   LCD COMMANDS USED WITH LCD_IOCTL
//
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
//
//   CLEAR DISPLAY
//
//////////////////////////////////////////////////////
#define LCD_CLEAR_DISPLAY        0x01
#define LCD_CLEAR_SCREEN         0x01

//////////////////////////////////////////////////////
//
//   RETURN HOME
//
//////////////////////////////////////////////////////
#define LCD_RETURN_HOME          0x02

//////////////////////////////////////////////////////
//
//   ENTRY MODE SET
//
//////////////////////////////////////////////////////
#define LCD_DECREMENT_DD_RAM     0x04
#define LCD_INCREMENT_DD_RAM     0x06
#define LCD_NO_SHIFT             0x04
#define LCD_SHIFT                0x05

//////////////////////////////////////////////////////
//
//   DISPLAY CONTROL COMMANDS
//
//////////////////////////////////////////////////////
#define LCD_DISPLAY_ON           0x0C
#define LCD_DISPLAY_OFF          0x08
#define LCD_CURSOR_ON            0x0A
#define LCD_CURSOR_OFF           0x08
#define LCD_BLINK_ON             0x09
#define LCD_BLINK_OFF            0x08

//////////////////////////////////////////////////////
//
//   CURSOR OR DISPLAY SHIFT
//
//////////////////////////////////////////////////////
#define LCD_SHIFT_CURSOR_LEFT    0x10
#define LCD_SHIFT_CURSOR_RIGHT   0x14
#define LCD_SHIFT_DISPLAY_LEFT   0x18
#define LCD_SHIFT_DISPLAY_RIGHT  0x1C

//////////////////////////////////////////////////////
//
//   FUNCTION SET
//
//////////////////////////////////////////////////////
#define LCD_8_BIT                0x30
#define LCD_4_BIT                0x20
#define LCD_2_LINE               0x28
#define LCD_1_LINE               0x20
#define LCD_5_10_DOTS            0x24
#define LCD_5_7_DOTS             0x20

//////////////////////////////////////////////////////
//
//   SET CG RAM ADDRESS
//
//   Logical OR with address
//
//////////////////////////////////////////////////////
#define LCD_SET_CG_RAM           0x40

#define LCD_CG_RAM_ZERO          0x00
#define LCD_CG_RAM_ONE           0x08
#define LCD_CG_RAM_TWO           0x10
#define LCD_CG_RAM_THREE         0x18
#define LCD_CG_RAM_FOUR          0x20
#define LCD_CG_RAM_FIVE          0x28
#define LCD_CG_RAM_SIX           0x30
#define LCD_CG_RAM_SEVEN         0x38

//////////////////////////////////////////////////////
//
//   SET DD RAM ADDRESS
//
//   Logical OR with address
//
//////////////////////////////////////////////////////
#define LCD_SET_DD_RAM           0x80

///\struct lcdSettngs
///\brief container for LCD settings
typedef struct {
   uint8_t width;         ///< width, in characters, of the LCD
   uint8_t height;         ///< height, in characters, of the LCD
   uint8_t doubleW;      ///< control number of writes for 4 and 8 bit modes
   uint8_t rs;            ///< Register Select state
   uint8_t mask;         ///< LCD control mask
   uint8_t enable;         ///< LCD enable bit
} lcd_settings_t;

uint8_t lcd_open(uint8_t, uint8_t, uint8_t);
uint8_t lcd_ioctl(const uint8_t);
uint8_t lcd_write(uint8_t);
uint8_t lcd_print(uint8_t, const char *);
uint8_t lcd_printf(uint8_t, const char *, ...);
uint8_t lcd_print_centered(uint8_t, char *);
uint8_t lcd_blank_line(uint8_t);
uint8_t lcd_switch_display(uint8_t);
uint8_t lcd_set_CG_RAM(uint8_t, uint8_t *);
uint8_t lcd_set_CG_RAM_P(uint8_t, const char *);
uint8_t lcd_set_CG_RAM_simple(const uint8_t, const uint8_t);

#endif //__IIC_CHAR_LCD_H__
