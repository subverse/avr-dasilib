/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///\file ipod.h
#ifndef __IPOD_H__
#define __IPOD_H__

#define IPOD_DETECT_PORT            PIND
#define IPOD_DETECT_PIN             3

#define GET_IPOD_TYPE               0x12
#define GET_IPOD_NAME               0x14
#define PRESELECT_MAIN_LIST         0x16
#define PRESELECT_X_NUMBER_N        0x17
#define GET_COUNT_OF                0x18
#define GET_NAMES_OF_X_FROM_N_TO_O  0x1A
#define GET_TIME_STATUS             0x1C
#define GET_CURRENT_POSITION        0x1E
#define GET_TITLE_OF                0x20
#define GET_ARTIST_OF               0x22
#define GET_ALBUM_OF                0x24
#define SET_POLLING_MODE            0x26
#define EXECUTE_PLAYLIST            0x28
#define AIR_COMMAND                 0x29
#define GET_SHUFFLEMODE             0x2C
#define SET_SHUFFLEMODE             0x2E
#define GET_REPEATMODE              0x2F
#define SET_REPEATMODE              0x31
#define COUNT_SONGS                 0x35
#define JUMP_TO_N                   0x37

#define START_POLLING               0x01
#define STOP_POLLING                0x00

#define SWITCH_MODE_NONE            0x60
#define SWITCH_MODE_VOICE           0x61
#define SWITCH_MODE_REMOTE          0x62
#define SWITCH_MODE_AIR             0x64
#define SWITCH_MODE_UM              0x68

//   AiR REMOTE MODE COMMANDS
#define AIR_COMMAND_PLAY_PAUSE      0x01
#define AIR_COMMAND_STOP            0x02
#define AIR_COMMAND_SKIP_FORWARD    0x03
#define AIR_COMMAND_SKIP_BACKWARD   0x04
#define AIR_COMMAND_FF              0x05
#define AIR_COMMAND_FB              0x06
#define AIR_COMMAND_STOP_FF_FB      0x07

//   SIMPLE REMOTE MODE COMMANDS
#define COMMAND_PLAY_PAUSE          0x01
#define COMMAND_VOLUME_UP           0x02
#define COMMAND_VOLUME_DOWN         0x04
#define COMMAND_SKIP_FORWARD        0x08
#define COMMAND_SKIP_BACKWARD       0x10
#define COMMAND_RELEASE             0x00

#define SHUFFLEMODE_OFF             0x00
#define SHUFFLEMODE_SONGS           0x01
#define SHUFFLEMODE_ALBUMS          0x02

#define REPEAT_OFF                  0x00
#define REPEAT_SONG                 0x01
#define REPEAT_ALL                  0x02

#define TYPE_PLAYLIST               0x01
#define TYPE_ARTIST                 0x02
#define TYPE_ALBUM                  0x03
#define TYPE_GENRE                  0x04
#define TYPE_SONG                   0x05
#define TYPE_COMPOSER               0x06

#define BUTTON_DEBOUNCE             10
#define MODE_BLOCK                  1
#define MODE_NO_BLOCK               2
#define MODE_NON_BLOCK              2

#define STATUS_STOPPED              0x00
#define STATUS_PLAYING              0x01
#define STATUS_PAUSED               0x02

#define CHAR_TIMEOUT                10000
#define IPOD_BUFSIZ                 30

typedef struct {
   uint16_t current;
   uint16_t total;
} ipod_playlist_t;

///\struct strTrackTimeS
///\brief total and current track time as strings
typedef struct {
   char total[6];
   char elapsed[6];
} ipod_track_time_str_t;

///\struct strTrackTime
///\brief total and current track time as integers
typedef struct {
   uint32_t total;
   uint32_t elapsed;

} ipod_track_time_t;

///\struct strIpod
///\brief container for iPod data
typedef struct {
   char artist[IPOD_BUFSIZ];   ///\< Artist name
   char title[IPOD_BUFSIZ];   ///\< Track title
   char album[IPOD_BUFSIZ];   ///\< Album name
   char name[IPOD_BUFSIZ];
   uint8_t type;            ///\< iPod type
   uint8_t size;            ///\< iPod size
   uint8_t airCommand;         ///\< AiR command to execute
   uint8_t status;            ///\< play,pause
   uint8_t shufflemode;      ///\< to set/get shuffle mode
   uint8_t repeatmode;         ///\< to set/get repeat mode
   uint16_t totalSongs;      ///\< total songs on device
   ipod_track_time_t trackTime;      ///\< current track time as integers
   ipod_track_time_str_t trackTimeS;   ///\< current track time as strings
   ipod_playlist_t playlist;         ///\< total and current playlist
   ipod_playlist_t song;            ///\< total and current song
} ipod_t;

//
//Commands supported by ipod_ioctl
//
typedef enum {
   IPOD_SWITCH_MODE_AIR = 0,
   IPOD_GET_TYPE,
   IPOD_GET_NAME,
   IPOD_GET_TIME_STATUS,
   IPOD_PRESELECT_MAIN,
   IPOD_GET_CURRENT_POSITION,
   IPOD_GET_SHUFFLEMODE,
   IPOD_GET_REPEATMODE,
   IPOD_COUNT_SONGS,
   IPOD_GET_COUNT_OF_PLAYLISTS,
   IPOD_GET_COUNT_OF_ARTISTS,
   IPOD_GET_COUNT_OF_ALBUMS,
   IPOD_GET_COUNT_OF_GENRES,
   IPOD_GET_COUNT_OF_SONGS,
   IPOD_GET_COUNT_OF_COMPOSERS,
   IPOD_SET_POLLINGMODE,
   IPOD_AIR_PLAYBACK,
   IPOD_SET_SHUFFLEMODE,
   IPOD_SET_REPEATMODE,
   IPOD_GET_TITLE_OF,
   IPOD_GET_ARTIST_OF,
   IPOD_GET_ALBUM_OF,
   IPOD_EXECUTE_PLAYLIST,
   IPOD_JUMP_TO,
   IPOD_PRESELECT_X_NUMBER_N,
   IPOD_GET_NAMES_OF_X,
   IPOD_PRESELECT_PLAYLIST,
   IPOD_IN_DOCK
} ipod_dispatch_table_index_t;

uint8_t ipod_open(ipod_t *);
uint8_t ipod_ioctl(ipod_t *, uint8_t);

//dev commands
uint8_t ipod_set_flag(ipod_t *);
uint8_t ipod_read_flag(ipod_t *);

#endif
