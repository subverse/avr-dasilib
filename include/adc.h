/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __ADC_H__
#define __ADC_H__

#define ADC_CHANNEL_1            0
#define ADC_CHANNEL_2            1
#define ADC_CHANNEL_3            2
#define ADC_CHANNEL_4            3
#define ADC_CHANNEL_5            4
#define ADC_CHANNEL_6            5
#define ADC_CHANNEL_7            6
#define ADC_CHANNEL_8            7

#define ADC_MAX_CHANNELS         8

///\struct adc_result_t
///\brief container for ADC results
typedef struct {
   uint8_t channel[ADC_MAX_CHANNELS];
} adc_result_t;

void adc_init(void);
void adc_start(uint8_t num);
uint8_t adc_read(uint8_t num);

#endif /*__ADC_H__*/
