/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __IIC_H__
#define __IIC_H__

//Real Time Clock + 240 * 8 RAM
#define PCF8583               0xA0
//LCD segment driver
#define PCF8577               0x74
//LCD segment driver
#define PCF8576               0x70
//Real Time Clock
#define DS1307                0xD0

#define IIC_WRITE             0xFE
#define IIC_READ              0x01
#define IIC_ADDRESS_0         0x00
#define IIC_ADDRESS_1         0x02
#define IIC_ADDRESS_2         0x04
#define IIC_ADDRESS_3         0x06
#define IIC_ADDRESS_4         0x08
#define IIC_ADDRESS_5         0x0A
#define IIC_ADDRESS_6         0x0D
#define IIC_ADDRESS_7         0x0E

#define MT_SLA_ACK            0x18
#define MT_DATA_ACK           0x28
#define TW_START              0x08
#define NACK                  0
#define ACK                   1

uint8_t iic_init(void);
uint8_t iic_start(void);
uint8_t iic_stop(void);
uint8_t iic_write_byte(uint8_t);
uint8_t iic_read_byte(uint8_t *, uint8_t);
uint8_t iic_write(uint8_t, uint8_t, uint8_t *, uint8_t);
uint8_t iic_read(uint8_t, uint8_t, uint8_t *, uint8_t);

#endif
