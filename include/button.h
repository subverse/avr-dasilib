/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __BUTTON_H__
#define __BUTTON_H__

#define BUTTON_DEBOUNCE             10

#define IIC_BUTTONS                 IIC_ADDRESS_0
#define RAW_BUTTON_NONE             0xFF
#define RAW_BUTTON_ERROR            0x7F
#define RAW_BUTTON_RELEASE          0x3F

#define RAW_BUTTON_PLAY_PAUSE       0xFB
#define RAW_BUTTON_VOLUME_UP        0xFE
#define RAW_BUTTON_VOLUME_DOWN      0xEF
#define RAW_BUTTON_SKIP_FORWARD     0xBF
#define RAW_BUTTON_SKIP_BACKWARD    0xFA
#define RAW_BUTTON_MENU             0xDF

#define BUTTON_NONE                 0x00
#define BUTTON_PLAY_PAUSE           0x01
#define BUTTON_VOLUME_UP            0x02
#define BUTTON_VOLUME_DOWN          0x03
#define BUTTON_SKIP_FORWARD         0x04
#define BUTTON_SKIP_BACKWARD        0x05
#define BUTTON_MENU                 0x06
#define BUTTON_RELEASE              0x07
#define BUTTON_ERROR                0x08
#define BUTTON_TIMEOUT              0x09
#define BUTTON_PLAYLIST_NEXT        0x0A
#define BUTTON_PLAYLIST_PREV        0x0B

uint8_t button_get(uint16_t);

#endif /*__BUTTON_H__*/
