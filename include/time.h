/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * time.h
 *
 *  Created on: 15/03/2009
 *      Author: dave
 */

#ifndef TIME_H_
#define TIME_H_

struct tm {
   int tm_sec;                 // seconds after the minute - [0,59]
   int tm_min;                 // minutes after the hour - [0,59]
   int tm_hour;                // hours since midnight - [0,23]
   int tm_mday;                // day of the month - [1,31]
   int tm_mon;                 // months since January - [0,11]
   int tm_year;                // years since 1900
   int tm_wday;                // days since Sunday - [0,6]
   int tm_yday;                // days since January 1 - [0,365]
   int tm_isdst;               // daylight savings time flag
};

#endif /* TIME_H_ */
