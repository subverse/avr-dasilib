/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __GAUGE_H__
#define __GAUGE_H__

#define GAUGE_WTR_TEMP    IIC_ADDRESS_0
#define GAUGE_OIL_TEMP    IIC_ADDRESS_1
#define GAUGE_MOOD_RING   IIC_ADDRESS_2
#define GAUGE_AFR         IIC_ADDRESS_4

uint8_t gauge_init(void);
uint8_t gauge_write(uint8_t gauge, uint8_t value);

#endif /*__GAUGE_H__*/
