/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __SCI_H__
#define __SCI_H__

#define TX_BUFSIZ         20
#define RX_BUFSIZ         40

typedef enum {
   WAITING_FOR_FF,
   WAITING_FOR_55,
   WAITING_FOR_LENGTH,
   GETTING_MESSAGE,
   WAITING_FOR_CHECKSUM,
   RX_FINISHED,
   RX_ERROR
} ipod_rx_state_t;

typedef struct {
   ipod_rx_state_t state;
   uint8_t length;
   uint8_t tmplen;
   uint8_t checksum;
   circular_buffer_t buffer;
} ipod_receiver_t;

typedef enum {
   SEND_FF,
   SEND_55,
   SEND_LENGTH,
   SEND_MESSAGE,
   SEND_CHECKSUM,
   TX_FINISHED,
   TX_ERROR
} ipod_tx_state_t;

typedef struct {
   ipod_tx_state_t state;
   uint8_t length;
   uint8_t index;
   uint8_t checksum;
   uint8_t buffer[TX_BUFSIZ];
} ipod_transmitter_t;

uint8_t ipod_sci_init(void);
uint8_t ipod_sci_get_char(uint8_t *);
uint8_t ipod_sci_put_char(uint8_t);
uint8_t ipod_sci_write(void);
uint8_t ipod_sci_flush_buffer(void);
uint8_t *ipod_sct_get_tx_buffer(void);

#endif
