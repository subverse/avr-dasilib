/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __BARGRAPH_H__
#define __BARGRAPH_H__

#define BAR_NONE        0
#define BAR_ONE         1
#define BAR_TWO         2
#define BAR_THREE       3
#define BAR_FOUR        4
#define BAR_FIVE        5

///\struct barSettings
///\brief container for bar graph settings
typedef struct {
   uint8_t width;      ///< number of LCD characters in bar graph
   uint8_t max;      ///< maximum number that can be shown by bar graph
   uint8_t bar_weight;    ///< value of each bar
   uint8_t char_weight;  ///< value of each LCD character
} bargraph_settings_t;

uint8_t bar_init(uint8_t, uint8_t);
uint8_t bar_write(uint8_t, uint8_t);

#endif
