/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __KWP_LAYER_2_H__
#define __KWP_LAYER_2_H__

#define K_LINE_PORT           PORTD
#define K_LINE_DDR            DDRD
#define K_LINE_PIN            PD1

#define K_LINE_ASSERT()       {K_LINE_PORT &= ~_BV(K_LINE_PIN);}
#define K_LINE_RELEASE()      {K_LINE_PORT |=  _BV(K_LINE_PIN);}
//#define K_LINE_ASSERT()        { K_LINE_PORT |=  _BV(K_LINE_PIN); }
//#define K_LINE_RELEASE()      { K_LINE_PORT &= ~_BV(K_LINE_PIN); }

#define L_LINE_PORT           PORTD
#define L_LINE_PIN            PD2

#define L_LINE_ASSERT()       {L_LINE_PORT |=  _BV(L_LINE_PIN);}
#define L_LINE_RELEASE()      {L_LINE_PORT &= ~_BV(L_LINE_PIN);}

/*
 * KWP 2000 Response Codes
 */
#define KWP_RC_GR             0x10  /* General Reject */
#define KWP_RC_SNS            0x11  /* Service Not Supported */
#define KWP_RC_SFNS_IF        0x12  /* Sub Function Not Supported - Invalid Format */
#define KWP_RC_B_RR           0x21  /* Busy - Repeat Request */
#define KWP_RC_CNCORSE        0x22  /* conditions Not Correct Or Request Sequence Error */
#define KWP_RC_RNC            0x23  /* Routine Not Complete Or Service In Progress */
#define KWP_RC_ROOR           0x31  /* Request Out Of Range */
#define KWP_RC_SAD_SAR        0x33  /* Security Access Denied - Security Access Requested */
#define KWP_RC_IK             0x35  /* Invalid Key */
#define KWP_RC_ENOA           0x36  /* Exceeded Number Of Attempts */
#define KWP_RC_RTDNE          0x37  /* Required Time Delay Not Expired */
#define KWP_RC_DNA            0x40  /* Download Not Accepted */
#define KWP_RC_IDT            0x41  /* Improper Download Type */
#define KWP_RC_CNDTSA         0x42  /* Can Not Download To Specified Address */
#define KWP_RC_CNDNOBR        0x43  /* Can Not Download Number Of Bytes Requested */
#define KWP_RC_UNA            0x50  /* Upload Not Accepted */
#define KWP_RC_IUT            0x51  /* Improper Upload Type */
#define KWP_RC_CNUFSA         0x52  /* Can Not Upload From Specified Address */
#define KWP_RC_CNUNOBR        0x53  /* Can Not Upload Number Of Bytes Requested */
#define KWP_RC_TS             0x71  /* Transfer Suspended */
#define KWP_RC_TA             0x72  /* Transfer Aborted */
#define KWP_RC_IAIBT          0x74  /* Illegal Address In Block Transfer */
#define KWP_RC_IBCIBT         0x75  /* Illegal Byte Count In Block Transfer */
#define KWP_RC_IBTT           0x76  /* Illegal Block Transfer Type */
#define KWP_RC_BTDCE          0x77  /* Block Transfer Data Checksum Error */
#define KWP_RC_RCR_RP         0x78  /* Request Correctly Received - Response Pending */
#define KWP_RC_IBCDBT         0x79  /* Incorrect Byte Count During Block Transfer */
#define KWP_RC_SNSIADS        0x80  /* Service Not Supported In Active Diagnostic Session */

/*  0x00    Reserved By Document */
/*  0x81 - 0x8F Reserved By Document */
/*  0x90 - 0xF9  Vehicle Manufacturer Specific */
/*  0xFA - 0xFE  System Supplier Specific */
/*  0xFF    Reserved By Document */

/*
 *   KWP 2000 Unit and Format Identifiers
 */
#define KWP_UNIT_NONE         0x00
#define KWP_METER             0x01
#define KWP_FOOT              0x02
#define KWP_INCH              0x03
#define KWP_YARD              0x04
#define KWP_UK_MILE           0x05
#define KWP_GRAM              0x06
#define KWP_METRIC_TON        0x07
#define KWP_SECOND            0x08
#define KWP_MINUTE            0x09
#define KWP_HOUR              0x0A
#define KWP_DAY               0x0B
#define KWP_YEAR              0x0C
#define KWP_AMPERE            0x0D
#define KWP_VOLT              0x0E
#define KWP_COULOMB           0x0F
#define KWP_OHM               0x10
#define KWP_FARAD             0x11
#define KWP_HENRY             0x12
#define KWP_SEIMENS           0x13
#define KWP_WEBER             0x14
#define KWP_TESLA             0x15
#define KWP_KELVIN            0x16
#define KWP_CELSIUS           0x17
#define KWP_FAHRENHEIT        0x18
#define KWP_CANDELA           0x19
#define KWP_RADIAN            0x1A
#define KWP_DEGREE            0x1B
#define KWP_HERTZ             0x1C
#define KWP_JOULE             0x1D
#define KWP_NEWTON            0x1E
#define KWP_KILOPOND          0x1F
#define KWP_POUND_FORCE       0x20
#define KWP_WATT              0x21
#define KWP_HP_METRIC         0x22
#define KWP_HP_IMPERIAL       0x23
#define KWP_PASCAL            0x24
#define KWP_BAR               0x25
#define KWP_ATMOSPHERE        0x26
#define KWP_PSI               0x27
#define KWP_BECQEREL          0x28
#define KWP_LUMEN             0x29
#define KWP_LUX               0x2A
#define KWP_LITER             0x2B
#define KWP_UK_GALLON         0x2C
#define KWP_US_GALLON         0x2D
#define KWP_CUBIC_INCH        0x2E
#define KWP_METER_PER_SECOND  0x2F
#define KWP_KILOMETER_PER_HOUR      0x30
#define KWP_MILE_PER_HOUR           0x31
#define KWP_REVOLUTIONS_PER_SECOND  0x32
#define KWP_REVOLUTIONS_PER_MINUTE  0x33
#define KWP_RPM               0x33
#define KWP_COUNTS            0x34
#define KWP_PERCENT           0x35
#define KWP_MILLIGRAM_PER_STROKE    0x36
#define KWP_METER_PER_SRUARE_SECOND 0x37
#define KWP_NEWTON_METER      0x38

/*
 * Prefixes
 */
#define KWP_EXA               0x40
#define KWP_PETA              0x41
#define KWP_TERA              0x42
#define KWP_GIGA              0x43
#define KWP_MEGA              0x44
#define KWP_KILO              0x45
#define KWP_HECTO             0x46
#define KWP_DECA              0x47
#define KWP_DECI              0x48
#define KWP_CENTI             0x49
#define KWP_MILLI             0x4A
#define KWP_MICRO             0x4B
#define KWP_NANO              0x4C
#define KWP_PICO              0x4D
#define KWP_FEMTO             0x4E
#define KWP_ATTO              0x4F
#define KWP_DATE_1            0x50
#define KWP_DATE_2            0x51
#define KWP_DATE_3            0x52
#define KWP_WEEK              0x53
#define KWP_TIME_1            0x54
#define KWP_TIME_2            0x55
#define KWP_DATE_AND_TIME_1   0x56
#define KWP_DATE_AND_TIME_2   0x57

/*
 * KWP 2000 Service Identifier Values
 */
#define KWP_SID_START_COMMUNICATIONS                  0x81
#define KWP_SID_STOP_COMMUNICATIONS                   0x82
#define KWP_SID_ACCESS_TIMING_PARAMETERS              0x83
//#define KWP_SID_TESTER_PRESENT                      0x3E
//#define KWP_SID_START_DIAGNOSTIC_SESSION            0x10
#define KWP_SID_STOP_DIAGNOSTIC_SESSION               0x20
//#define KWP_SID_SECURITY_ACCESS                     0x27
//#define KWP_SID_ECU_RESET                           0x11
//#define KWP_SID_READ_ECU_ID                         0x1A
//#define KWP_SID_READ_ECU_IDENTIFIER                 0x1A
#define KWP_SID_READ_DATA_BY_LOCAL_ID                 0x21
#define KWP_SID_READ_DATA_BY_LOCAL_IDENTIFIER         0x21
#define KWP_SID_READ_DATA_BY_COMMON_ID                0x22
#define KWP_SID_READ_DATA_BY_COMMON_IDENTIFIER        0x22
#define KWP_SID_READ_MEMORY_BY_ADDRESS                0x23
#define KWP_SID_DYNAMICALLY_DEFINE_LOCAL_ID           0x2C
#define KWP_SID_DYNAMICALLY_DEFINE_LOCAL_IDENTIFIER   0x2C
#define KWP_SID_WRITE_DATA_BY_LOCAL_ID                0x3B
#define KWP_SID_WRITE_DATA_BY_LOCAL_IDENTIFIER        0x3B
#define KWP_SID_WRITE_DATA_BY_COMMON_ID               0x2E
#define KWP_SID_WRITE_DATA_BY_COMMON_IDENTIFIER       0x2E
#define KWP_SID_WRITE_DATA_BY_ADDRESS                 0x3D
#define KWP_SID_SET_DATA_RATES                        0x26
#define KWP_SID_STOP_REPEATED_DATA_TRANSMISSION       0x25
#define KWP_SID_READ_DIAGNOSTIC_TROUBLE_CODES         0x13
#define KWP_SID_READ_DIAGNOSTIC_TROUBLE_CODES_BY_STATUS    0x18
#define KWP_SID_READ_STATUS_OF_DIAGNOSTIC_TROUBLE_CODES    0x17
#define KWP_SID_READ_FREEZE_FRAME_DATA                0x12
#define KWP_SID_CLEAR_DIAGNOSTIC_INFORMATION          0x14
#define KWP_SID_IO_CONTROL_BY_LOCAL_ID                0x30
#define KWP_SID_IO_CONTROL_BY_LOCAL_IDENTIFIER        0x30
#define KWP_SID_IO_CONTROL_BY_COMMON_ID               0x2F
#define KWP_SID_IO_CONTROL_BY_COMMON_IDENTIFIER       0x2F
#define KWP_SID_START_ROUTINE_BY_LOCAL_ID             0x31
#define KWP_SID_START_ROUTINE_BY_LOCAL_IDENTIFIER     0x31
#define KWP_SID_START_ROUTINE_BY_ADDRESS              0x38
#define KWP_SID_STOP_ROUTINE_BY_LOCAL_ID              0x32
#define KWP_SID_STOP_ROUTINE_BY_LOCAL_IDENTIFIER      0x32
#define KWP_SID_STOP_ROUTINE_BY_ADDRESS               0x39
#define KWP_SID_REQUEST_ROUTINE_RESULTS_BY_LOCAL_ID   0x33
#define KWP_SID_REQUEST_ROUTINE_RESULTS_BY_LOCAL_IDENTIFIER  0x33
#define KWP_SID_REQUEST_ROUTINE_RESULTS_BY_ADDRESS    0x3A
#define KWP_SID_REQUEST_DOWNLOAD                      0x34
#define KWP_SID_REQUEST_UPLOAD                        0x35
#define KWP_SID_TRANSFER_DATA                         0x36
#define KWP_SID_REQUEST_TRANSFER_EXIT                 0x37

/*
 * KWP2000 Service ID Values and Parameter Values
 */

/* Start Communication Request */
#define KWP_SCR              0x81

/*
 * Section 6: Diagnostic Management Functional Unit
 */

/*
 * KWP2000 6.1 Start Diagnostic Session Service
 */
#define KWP_START_DIAGNOSTIC_SESSION  0x10
#define KWP_STDS              0x10
#define KWP_STDSPR            0x50  /* Positive Response */
/*
 * Start Diagnostic Session (DCS_) Parameters
 */
#define KWP_DCS_SS            0x81  /* Standard Session */
#define KWP_DCS_PT            0x82  /* Periodic Transmissions */
#define KWP_DCS_PS            0x85  /* Programming Session */
#define KWP_DCS_DS            0x86  /* Development Session */
#define KWP_DCS_AS            0x87  /* Adjustment Session */

/*
 * KWP2000 6.2 Stop Diagnostic Session Service
 */

/*
 * KWP2000 6.3 Security Access Service
 */
#define KWP_SECURITY_ACCESS   0x27
#define KWP_SA                0x27
#define KWP_SAPR              0x67  /* Positive Response */
/*
 * Security Access Service, Access Mode (AM_) Parameters
 */
#define KWP_AM_RSD            0x01  /* Request Seed (with default security level) */
#define KWP_AM_SK             0x02  /* Send Key (with default security level) */
/*
 * Security Access Service, Security Access Status (SAS_) Parameter
 */
#define KWP_SAS_SAA           0x34  /* Security Access Allowed */

/*
 * KWP2000 6.4 Tester Present Service
 */
#define KWP_TESTER_PRESENT    0x3E
#define KWP_TP                0x3E
#define KWP_TPPR              0x7E  /* Positive Response */

/*
 * KWP2000 6.5 ECU Reset Service
 */
#define KWP_ECU_RESET         0x11
#define KWP_ER                0x11
#define KWP_ERPR              0x51  /* Positive Response */
/*
 * ECU Reset Service, Reset Mode (RM_) Parameters
 */
#define KWP_RM_PO             0x01  /* Power On */
#define KWP_RM_KO             0x03  /* Key On */
#define KWP_RM_RS             0x80  /* Send Reset Status */

/*
 * KWP2000 6.6 Read ECU ID Service
 */
#define KWP_READ_ECU_ID       0x1A
#define KWP_REI               0x1A
#define KWP_REIPR             0x5A  /* Positive Response */
/*
 * Read ECU ID Service, Identification Option (IO_) Parameters
 */
#define KWP_IO_ECUIDT         0x80  /* ECU ID Data Table */
#define KWP_IO_ECUIST         0x81  /* EDU ID Scaling Table */
#define KWP_IO_VMSPN          0x87  /* VM (Vehicle Manufacturer) Part Number */
#define KWP_IO_VMECUSN        0x88  /* VM ECU Software Number */
#define KWP_IO_VMECUSVN       0x89  /* VM ECU Software Version Number */
#define KWP_IO_SS             0x8A  /* SS (System Supplier) */
#define KWP_IO_ECUMD          0x8B  /* ECU Manufacture Date */
#define KWP_IO_ECUSN          0x8C  /* ECU Serial Number */
#define KWP_IO_VIN            0x90  /* VIN */
#define KWP_IO_VMECUHN        0x91  /* VM ECU Hardware Number */
#define KWP_IO_SSECUHN        0x92  /* SS ECU Hardware Number */
#define KWP_IO_SSECUHVN       0x93  /* SS ECU Hardware Version Number */
#define KWP_IO_SSECUSN        0x94  /* SS ECU Software Number */
#define KWP_IO_SSECUSVN       0x95  /* SS ECU Software Version Number */
#define KWP_IO_EROTAN         0x96  /* Exhaust Regulation or Type Approval Number */
#define KWP_IO_SNOET          0x97  /* System Name or Engine Type */
#define KWP_IO_RSCOTSN        0x98  /* Repair Shop Code or Tester Serial Number */
#define KWP_IO_PD             0x99  /* Programming Date */
#define KWP_IO_CRSCOCESN      0x9A  /* Calibration Repair Shop Code or Equipment Serial Number */
#define KWP_IO_CD             0x9B  /* Calibration Date */
#define KWP_IO_CESN           0x9C  /* Calibration Equipment Software Number */
#define KWP_IO_ECUID          0x9D  /* ECU Installation Date */

/*
 * Section 7: Data Transmission Functional Unit
 */

/*
 * KWP2000 7.1 Read Data by Local Identifier Service
 */
#define KWP_RDBLI             0x21
#define KWP_RDBLIPR           0x61  /* Positive Response */
/*
 * Read Data by Local Identifier, Record Local Identifier (RLI_) Parameters
 */
#define KWP_RLI_LIST          0x01  /* Local Identifier Scaling Table */
/* 0x02 - 0xEF Local Identifier */
/* 0xF0 - 0xF9 Dynamically Defined Local Identifier */
/* 0xFA - 0xFE System Supplier Specific */

#define KWP_          0x00  /*  */

#define KWP1281_TYPE_CLEAR_ERROR        0x05
#define KWP1281_TYPE_END_OUTPUT         0x06
#define KWP1281_TYPE_GET_ERROR          0x07
#define KWP1281_TYPE_ACK                0x09
#define KWP1281_TYPE_BLOCK_READ         0x29
#define KWP1281_TYPE_BLOCK_ACK          0xE7
#define KWP1281_TYPE_ASCII              0xF6
#define KWP1281_TYPE_GET_ERROR_ACK      0xFC
#define KWP1281_MESSAGE_END             0x03

#define KWP_COMMAND_CLEAR_ERROR     KWP1281_TYPE_CLEAR_ERROR
#define KWP_COMMAND_GET_ERROR       KWP1281_TYPE_GET_ERROR

#define KWP_MAX_PENDING_COMMANDS    200
#define KWP_GROUP_LENGTH            12
#define KWP_ASCII_LENGTH            16

#define KWP_MAX_DATA_LENGTH         255
#define KWP_MAX_COMMAND_LENGTH      50

#define KWP1281_KB1 0x01
#define KWP1281_KB2 0x8A

#define PROTOCOL_UNKNOWN 0
#define KWP1281     1
#define KWP2000     2

///\struct kwp_header_t
///\brief KWP2000 (ISO 14230) Layer 2 header
typedef struct {
   uint8_t format;  ///<Addressing format and length
   uint8_t target;  ///<Target Address
   uint8_t source;  ///<Source Address
   uint8_t length;
   uint8_t checksum;
} kwp_header_t;

///\struct kwp1281_header_t
///\brief KWP1281 Layer 2 header
typedef struct {
   uint8_t length;
   uint8_t count;
   uint8_t type;
} kwp1281_header_t;

///\struct kwp_command_t
///\brief kwp2000 service request
typedef struct {
   kwp_header_t header;
   uint8_t sid;      ///<Service ID
   uint8_t index;
   uint8_t data[KWP_MAX_COMMAND_LENGTH];
} kwp_command_t;

///\struct kwp_message_t
///\brief KWP2000 (ISO 14230) Layer 3 message
typedef struct {
   kwp_header_t header;
   circular_buffer_t data;
} kwp_message_t;

///\struct kwp1281_message_t
///\brief message received from ecu
typedef struct {
   kwp1281_header_t header;
   circular_buffer_t data;
} kwp1281_message_t;

///\struct kwp1281_command_t
///\brief command sent to ECU
typedef struct {
   kwp1281_header_t header;
   uint8_t parameter;
} kwp1281_command_t;

///\struct kwp_tx_t
///\brief Transmitter state data
typedef struct {
   uint8_t state;
} kwp_tx_t;

///\struct kwp_rx_t
///\brief Receiver state data
typedef struct  {
   uint8_t state;
} kwp_rx_t;

///\struct kwp_ecu
///\brief model of remote ecu
typedef struct {
   kwp_rx_t rx;      ///< transmitter state
   kwp_tx_t tx;      ///< receiver state
   uint8_t address;  ///< ecu address
   uint8_t kb1;      ///< keybyte 1
   uint8_t kb2;      ///< keybyte 2
   uint8_t protocol;
   uint8_t pending;  ///< true when command pending
   uint8_t state;
} kwp_ecu_t;

///\brief ecu states also used as index to array of UI messages about ecu
typedef enum {
   ECU_OFFLINE = 0,
   ECU_CONNECTING,
   ECU_ONLINE,
   BUFFER_FULL
} ecu_state_t;

///\brief Receiver states
typedef enum {
   RX_GET_KB1,
   RX_GET_KB2,
   RX_GET_LENGTH,
   RX_GET_COUNTER,
   RX_GET_TYPE,
   RX_GET_BLOCK,
   RX_GET_ASCII,
   RX_GET_ERROR,
   RX_GET_END,
   RX_GET_ECHO,
   PROTOCOL_ERROR,
   RX_ERROR,
   RX_FINISHED,
   RX_GET_ADDRESS,
   RX_GET_FMT,
   RX_GET_SRC,
   RX_GET_TGT,
   RX_GET_LEN,
   RX_GET_DATA,
   RX_GET_CS
} kwp_rx_state_t;

///\brief Transmitter states
typedef enum {
   TX_SEND_FMT,
   TX_SEND_TGT,
   TX_SEND_SRC,
   TX_SEND_LEN,
   TX_SEND_SID,
   TX_SEND_DATA,
   TX_SEND_CHECKSUM,
   TX_WAIT,
   TX_FINISHED,
   TX_ERROR,
   SEND_ACK,
   TX_SEND_LENGTH,
   TX_SEND_COUNT,
   TX_SEND_TYPE,
   TX_SEND_PARAM,
   TX_SEND_END,
   TX_DONE
} kwp_tx_state_t;

///\brief Timer overflow states
typedef enum {
   TOVF_SEND_START,
   TOVF_SEND_DATA,
   TOVF_SEND_PARITY,
   TOVF_SEND_STOP,
   TOVF_TX_FINISHED,
   TOVF_TX_ERROR,
   TOVF_TIMEOUT,
   TOVF_REPLY_DELAY
} timer_overflow_state_t;

///\brief Input capture states
typedef enum {
   IC_IDLE_WAIT,
   IC_BAUD_SET,
   IC_AUTO_BAUD
} input_capture_state_t;

typedef struct  {
   uint8_t state;
   uint8_t data;
} timer_overflow_t;

typedef struct  {
   uint8_t state;
} input_capture_t;

uint8_t kwp_init_device_fast(uint8_t);
uint8_t kwp_5b_init_device(uint8_t);
uint8_t kwp_5b_get_tx_state(void);
uint8_t kwp_init_usart(void);
uint8_t kwp_get_char(uint8_t *);
uint8_t kwp_message_ready(void);
uint8_t kwp_get_ecu_state(void);
uint8_t kwp_get_ecu_protocol(void);
uint8_t kwp_queue_command(uint8_t);
uint8_t kwp_request_block(uint8_t);
uint8_t kwp_read_block(uint8_t *);
uint8_t kwp_read_ascii(uint8_t *);
uint8_t kwp_start_communication(uint8_t);

#endif /*__KWP_H__*/
