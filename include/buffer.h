/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __BUFFER_H__
#define __BUFFER_H__

#define BUFSIZ            200

typedef struct {
   uint8_t head;            // index to head of buffer
   uint8_t tail;            // index to tail of buffer
   uint8_t buffer[BUFSIZ];   // buffer
} circular_buffer_t;

uint8_t buffer_init(circular_buffer_t *);
uint8_t buffer_get(uint8_t *, circular_buffer_t *);
uint8_t buffer_put(uint8_t, circular_buffer_t *);

#endif /* __BUFFER_H__ */
