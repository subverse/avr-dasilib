/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __IIC_CLOCK_H__
#define __IIC_CLOCK_H__

uint8_t bcd_to_hex(uint8_t);
uint8_t hex_to_bcd(uint8_t);
uint8_t iic_get_time(struct tm *);
uint8_t iic_set_time(struct tm *);

#endif
