/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __STV5730_H__
#define __STV5730_H__

#define STV_PAL            0x00
#define STV_NTSC           0x01

#define STV_ROW_ONE        0
#define STV_ROW_TWO        1
#define STV_ROW_THREE      2
#define STV_ROW_FOUR       3
#define STV_ROW_FIVE       4
#define STV_ROW_SIX        5
#define STV_ROW_SEVEN      6
#define STV_ROW_EIGHT      7
#define STV_ROW_NINE       8
#define STV_ROW_TEN        9
#define STV_ROW_ELEVEN     10

#define STV_MAX_ROWS       11
#define STV_MAX_COLUMNS    28

#define STV_COLOUR_BLACK   0x00
#define STV_COLOUR_BLUE    0x01
#define STV_COLOUR_GREEN   0x02
#define STV_COLOUR_CYAN    0x03
#define STV_COLOUR_RED     0x04
#define STV_COLOUR_MAGENTA 0x05
#define STV_COLOUR_YELLOW  0x06
#define STV_COLOUR_WHITE   0x07

#define STV_SPACE_16B      0x000B

typedef struct {
   uint8_t font;
   uint8_t border;
   uint8_t background;
} character_colour_t;

typedef struct {
   uint8_t background;
   character_colour_t character;
} colour_t;

typedef struct {
   uint16_t row;
   uint16_t page;
   uint16_t character;
   colour_t colour;
} video_attributes_t;

uint8_t stv_reset(uint8_t);
uint8_t stv_write(char *, uint8_t);
uint8_t stv_write_centered(char *, uint8_t);
uint8_t stv_blank_line(uint8_t);
uint8_t stv_clear_screen(void);
uint8_t stv_set_position(uint8_t, uint8_t);
uint8_t stv_set_character_colour(uint8_t);
uint8_t stv_set_background_colour(uint8_t);
uint8_t stv_set_character_border_colour(uint8_t);
uint8_t stv_set_character_background_colour(uint8_t);
uint8_t stv_set_page_attributes(void);
uint8_t stv_blink_enable(void);
uint8_t stv_blink_disable(void);

#endif /*STV5730_H_*/
