/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __DS1807_H__
#define __DS1807_H__

#define VOLUME_DOWN          0
#define VOLUME_UP            1
#define CHANNEL_LEFT         0
#define CHANNEL_RIGHT        1
#define CHANNEL_BOTH         2

//Digital Potentiometer
#define DS1807               0x50

uint8_t iic_pot_read(uint8_t, uint16_t *);
uint8_t iic_pot_write(uint8_t, uint16_t);
uint8_t ds_set_volume(uint8_t, uint8_t);
uint8_t ds_get_volume(uint16_t *);

#endif /*DS1807_H_*/
