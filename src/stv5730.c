/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\file      stv5730.c
///
///\brief      Driver for STV5730 Multi-standard on-screen display
///            from SGS-THOMSON MICROELECTRONICS - ST.
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version      1.1 10/10/2010
///            - formatted for doxygen
///
///\version      1.0
///
///   Copyright (C) 2006 David McKelvie
///
/////////////////////////////////////////////////////////////////////////////
#include <avr/pgmspace.h>
#include <string.h>
#include <stdint.h>
#include <util/delay.h>   //needs F_CPU defined
#include "spi.h"
#include "stv5730.h"

//convert ascii characters to stv5730 characters
static const char ascii2stv5730[] PROGMEM = {0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C,
      0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F,
      0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x29, 0x0B, 0x0B, 0x5F, 0x5E, 0x29, 0x0A, 0x27, 0x28, 0x00, 0x01, 0x02,
      0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x26, 0x0B, 0x7B, 0x78, 0x7A, 0x70, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
      0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23,
      0x24, 0x25, 0x63, 0x0B, 0x60, 0x0B, 0x0B, 0x0B, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x34,
      0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F, 0x40, 0x41, 0x42, 0x43, 0x0B, 0x0B, 0x0B, 0x0B,
      0x0B};

static video_attributes_t attributes;

///////////////////////////////////////////////////////////////
///
///\brief Reset STV5730
///
///////////////////////////////////////////////////////////////
uint8_t stv_reset(uint8_t system)
{
   uint8_t i;

   attributes.row = 0x18E0;         //set default row attributes
   attributes.character = 0x1700;         //set default character attributes
   attributes.colour.background = STV_COLOUR_BLACK;
   attributes.colour.character.font = STV_COLOUR_WHITE;
   attributes.colour.character.border = STV_COLOUR_BLACK;
   attributes.colour.character.background = STV_COLOUR_BLACK;

   spi_put_word(0x3000);            //reset sequence
   _delay_ms(1);
   spi_put_word(0x3000);
   _delay_ms(1);
   spi_put_word(0x00DB);
   _delay_ms(1);
   spi_put_word(0x1000);
   _delay_ms(1);
   spi_put_word(0x00CC);            //zoom address
   _delay_ms(1);
   spi_put_word(0x1000);            //Zoom attributes
   _delay_ms(1);
   spi_put_word(0x1000);            //Colour attributes
   _delay_ms(1);
   if (system == STV_PAL)
      spi_put_word(0x1BD5);         //Control attributes
   else
      spi_put_word(0x1AD5);
   _delay_ms(1);
   spi_put_word(0x1A14);            //Position attributes
   _delay_ms(1);
   spi_put_word(0x1036);            //Mode attributes
   _delay_ms(1);

   spi_put_word(0x00C0);            //Row attribute address
   _delay_ms(1);

   for (i = 0; i < 11; i++)
   {
      spi_put_word(attributes.row);   //row attributes
      _delay_ms(1);
   }

   return 1;
}

uint8_t stv_write(char *buffer, uint8_t len)
{
   uint8_t i, tmp;
   for (i = 0; i < len; i++)
   {
      tmp = buffer[i] & 0x7F;                              //get ascii character from buffer
      tmp = pgm_read_byte(ascii2stv5730 + tmp);                  //convert ascii character to stv5730
      spi_put_word(0x1000 | attributes.character | (uint16_t) tmp);    //combine with character attributes and send
      //spi_put_word( 0x1000 | (attributes.character | (uint16_t)( ascii2stv5730[ buffer[ i ] & 0x7F ] )) );
   }

   return len;
}

uint8_t stv_write_centered(char *buffer, uint8_t row)
{
   uint8_t length;
   length = strlen(buffer);
   if (length > STV_MAX_COLUMNS)
      length = STV_MAX_COLUMNS;
   stv_set_position(row, (STV_MAX_COLUMNS - length) >> 1);
   stv_write(buffer, length);
   return 1;
}

uint8_t stv_set_position(uint8_t row, uint8_t column)
{
   return spi_put_word((uint16_t) (row << 8) | (uint16_t) column);
}

uint8_t stv_set_background_colour(uint8_t colour)
{
   attributes.colour.background = colour;
   stv_set_page_attributes();
   return 1;
}

uint8_t stv_set_character_border_colour(uint8_t colour)
{
   attributes.colour.character.border = colour;
   stv_set_page_attributes();
   return 1;
}

uint8_t stv_set_character_background_colour(uint8_t colour)
{
   attributes.colour.character.background = colour;
   stv_set_page_attributes();
   return 1;
}

uint8_t stv_set_page_attributes(void)
{
   attributes.page = (uint16_t) ((attributes.colour.background) << 9);
   attributes.page |= (uint16_t) ((attributes.colour.character.border) << 6);
   attributes.page |= (uint16_t) (attributes.colour.character.background);
   spi_put_word(0x00CD);                     //Colour address
   _delay_ms(1);
   spi_put_word(0x1000 | attributes.page);      //Colour attributes
   _delay_ms(1);
   return 1;
}

uint8_t stv_set_character_colour(uint8_t colour)
{
   attributes.colour.character.font = colour;
   attributes.character &= 0xF8FF;
   attributes.character |= (uint16_t) (colour << 8);
   return 1;
}

uint8_t stv_blink_enable(void)
{
   attributes.character |= 0x0080;
   return 1;
}

uint8_t stv_blink_disable(void)
{
   attributes.character &= 0xFF7F;
   return 1;
}

uint8_t stv_clear_screen(void)
{
   uint8_t i, j;

   spi_put_word(0x0000);

   for (i = 0; i < 11; i++)
   {
      spi_put_word(0x0000 | ((uint16_t) (i << 8)));
      for (j = 0; j < 28; j++)
      {
         spi_put_word(attributes.character | STV_SPACE_16B);
      }
   }
   return 1;
}

uint8_t stv_blank_line(uint8_t line)
{
   uint8_t i;
   spi_put_word((uint16_t) (line << 8));
   for (i = 0; i < 28; i++)
      spi_put_word(attributes.character | STV_SPACE_16B);

   return 1;
}
