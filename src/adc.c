/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\file      adc.c
///
///\brief     AVR ADC driver
///
///\author    <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version   1.1 10/10/2010
///           - formatted for doxygen
///
///\version   1.0
///

#include <avr/interrupt.h>
#include <stdint.h>
#include "adc.h"

static volatile adc_result_t result;

///
///\brief  initialise ADC subsystem.
///
void adc_init(void)
{
   ADMUX = _BV(REFS0) | _BV(ADLAR);
   ADCSRA = _BV(ADEN) | _BV(ADIE);
   ADCSRA |= _BV(ADSC) | _BV(ADIF);  // start conversion, clear pending interrupt flag
}

///
///\brief Start an ADC conversion
///
///\param num ADC channel to start conversion on
///
void adc_start(uint8_t num)
{
   ADCSRA &= 0xF8;                       // clear channel select bits
   ADCSRA |= num;                        // set channel
   ADCSRA |= _BV(ADSC) | _BV(ADIF);  // start conversion, clear pending interrupt flag
}

///
///\brief Read the ADC conversion result
///
///\param num ADC channel to read
///
///\return result for given channel
///
uint8_t adc_read(uint8_t num)
{
   if (num < ADC_MAX_CHANNELS)
      return result.channel[num];
   return 0;
}

///
///\brief ADC ISR save ADC result, start conversion on next channel
///
ISR(ADC_vect)
{
   static uint8_t current_channel = 0;
   result.channel[current_channel] = ADCH;
   if (++current_channel == ADC_MAX_CHANNELS)
      current_channel = 0;
   ADCSRA &= 0xF8;               // clear channel select bits
   ADCSRA |= current_channel;    // select new channel
   ADCSRA |= _BV(ADSC);        // start new conversion
}
