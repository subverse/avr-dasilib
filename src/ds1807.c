/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\file   ds1807.c
///
///\brief   driver for ds1807 digital potentiometer
///
///\author   <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version   1.2 13/10/2010
///         - extracted pot read/write from iic.c
///
///\version   1.1 10/10/2010
///         - formatted for doxygen
///
///\version   1.0
///
#include <stdint.h>
#include "iic.h"
#include "ds1807.h"

///\todo make i2c address of pot set-able

///////////////////////////////////////////////////////////////
///
///\brief      read data from a digital potentiometer (DS1807)
///
///\param      address    address of I2C device as defined in
///                     iic.h ie IIC_ADDRESS_0.
///\param      data      pointer to uint16_t, where wiper position
///                     will be stored
///
///\verbatim
///   Device data bit map:
///
/// 15  14  13  12  11  10  9   8   7   6   5   4   3   2   1   0
///         ---------------------           ---------------------
/// x   m   |   wiper data 0    |   x   m   |   wiper data 1    |
///         ---------------------           ---------------------
///
/// x = don't care
/// m = mute
///
/// WIPER ->    HIGH    position 0
///             LOW     position 63
///             MUTE    position 64
///\endverbatim
///
///////////////////////////////////////////////////////////////
uint8_t iic_pot_read(uint8_t address, uint16_t *data)
{
   uint8_t tmp;
   iic_start();
   iic_write_byte(DS1807 | address | IIC_READ);
   iic_read_byte(&tmp, ACK);
   *data = (uint16_t) (tmp << 8);
   iic_read_byte(&tmp, NACK);
   *data |= (uint16_t) tmp;
   iic_stop();
   return 1;
}

///////////////////////////////////////////////////////////////
///
///\brief      write data to a digital potentiometer (DS1807)
///
///\param      address    address of I2C device as defined in
///                     iic.h ie IIC_ADDRESS_0.
///\param      data      uint16_t wiper data for device
///
///
///////////////////////////////////////////////////////////////
uint8_t iic_pot_write(uint8_t address, uint16_t data)
{
   iic_start();
   iic_write_byte((DS1807 | address) & IIC_WRITE);
   iic_write_byte(0xA9);
   iic_write_byte((uint8_t) (data >> 8));
   iic_write_byte((uint8_t) data);
   iic_stop();
   return 1;
}

///
///\brief get potentiometer value
///
///\param volume will be loaded with potentiometer value
///
///\return 1 on success
///
uint8_t ds_get_volume(uint16_t *volume)
{
   return iic_pot_read(IIC_ADDRESS_0, volume);
}

///
///\brief set potentiometer value
///
///\param channel CHANNEL_LEFT of CHANNEL_RIGHT
///
///\param direction VOLUME_UP or VOLUME_DOWN
///
///\return 1 on success
///
uint8_t ds_set_volume(uint8_t channel, uint8_t direction)
{
   uint16_t wipers;
   uint8_t tmp;
   iic_pot_read(IIC_ADDRESS_0, &wipers);         //get current wiper position

   switch (channel)
   {
      case CHANNEL_LEFT:                     //MSB
         tmp = (uint8_t) (wipers >> 8);
         if (direction == VOLUME_UP)
         {
            if (tmp)
               tmp--;
         }
         else
         {
            if (tmp < 63)
               tmp++;
         }
         wipers &= 0x00FF;
         wipers |= (uint16_t) (tmp << 8);
         break;

      case CHANNEL_RIGHT:                     //LSB
         tmp = (uint8_t) wipers;
         if (direction == VOLUME_UP)
         {
            if (tmp)
               tmp--;
         }
         else
         {
            if (tmp < 63)
               tmp++;
         }
         wipers &= 0xFF00;
         wipers |= (uint16_t) tmp;
         break;

      case CHANNEL_BOTH:
         tmp = (uint8_t) wipers;
         if (direction == VOLUME_UP)
         {
            if (tmp)
               --tmp;
         }
         else
         {
            if (tmp < 63)
               ++tmp;
         }
         wipers = (uint16_t) (tmp << 8);
         wipers |= (uint16_t) tmp;
         break;

      default:
         break;
   }

   return iic_pot_write(IIC_ADDRESS_0, wipers);
}
