/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file      iicclock.c
///
///\brief      Driver for i2c clock IC PCF8583
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version      1.1 10/10/2010
///            - formatted for doxygen
///
///\version      1.0 10/08/2004
///
///   Copyright (C) 2005 David McKelvie
///
/////////////////////////////////////////////////////////
#include <stdint.h>
#include "time.h"
#include "iic.h"
#include "iicclock.h"

///////////////////////////////////////////////////////////////
///
///\brief   read time from PCF8583
///
///\param   ptrTime pointer to tm structure, will be loaded with time
///
///\return   1 on success
///
///////////////////////////////////////////////////////////////
uint8_t iic_get_time(struct tm * ptrTime)
{
   uint8_t buffer[8];
   if (iic_read(PCF8583, 0, buffer, 5))
   {
      ptrTime->tm_sec = bcd_to_hex(buffer[2]);
      ptrTime->tm_min = bcd_to_hex(buffer[3]);
      ptrTime->tm_hour = bcd_to_hex(buffer[4] & 0x3F);
      //ptrTime->tm_mday = bcd_to_hex( buffer[ 5 ] & 0x3F );
      //ptrTime->tm_mon  = bcd_to_hex( buffer[ 6 ] & 0x1F );
      return 1;
   }
   return 0;
}

///////////////////////////////////////////////////////////////
///
///\brief   set time in PCF8583
///
///\param   ptrTime pointer to tm structure containing time to
///         write to PCF8583
///
///\return   1 on success
///
///////////////////////////////////////////////////////////////
uint8_t iic_set_time(struct tm * ptrTime)
{
   uint8_t buffer[8];

   buffer[0] = 0;                  //control register
   buffer[1] = 0;                  //1/10s and 1/100s
   buffer[2] = hex_to_bcd(ptrTime->tm_sec);
   buffer[3] = hex_to_bcd(ptrTime->tm_min);
   buffer[4] = hex_to_bcd(ptrTime->tm_hour);
   //buffer[ 5 ] = hex_to_bcd( ptrTime->tm_mday );
   //buffer[ 6 ] = hex_to_bcd( ptrTime->tm_mon );

   return iic_write(PCF8583, 0, buffer, 5);
}

///////////////////////////////////////////////////////////////
///
///\brief   convert Binary Coded Decimal to Hexidecimal
///
///\param   data BCD number to convert
///
///\return   Hexidecimal result
///
///////////////////////////////////////////////////////////////
uint8_t bcd_to_hex(uint8_t data)
{
   uint8_t tmp;
   tmp = data & 0x0F;         //mask 1s nibble 0 - 9
   data = data >> 4;         //shift 10's nibble
   while (data--)
      tmp += 10;
   //data = (( data >> 4 ) * 10);
   //tmp += data;
   return tmp;
}

///////////////////////////////////////////////////////////////
///
///\brief   convert Hexidecimal to Binary Coded Decimal
///
///\param   data Hexidecimal number to convert
///
///\return   BCD result
///
///////////////////////////////////////////////////////////////
uint8_t hex_to_bcd(uint8_t data)
{
   uint8_t tmp;
   tmp = data / 10;      //get 10s nibble
   data -= tmp * 10;      //remove 10s from data
   tmp = tmp << 4;      //shift 10s nibble into place
   tmp |= data;         //combine 10s and 1s nibble
   return tmp;
}
