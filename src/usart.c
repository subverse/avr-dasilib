/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file      usart.c
///
///\brief      AVR USART driver.
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version      1.0 05/05/2005
///
///   Copyright (C) 2005 David McKelvie
///
/////////////////////////////////////////////////////////
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include "buffer.h"
#include "usart.h"

#define   UART_BAUD      10400UL

typedef enum {
   WAITING_FOR_FF, WAITING_FOR_55, WAITING_FOR_LENGTH, GETTING_MESSAGE, WAITING_FOR_CHECKSUM, RX_FINISHED, RX_ERROR
} ipod_rx_state_t;

typedef struct {

   ipod_rx_state_t state;
   uint8_t length;
   uint8_t tmplen;
   uint8_t checksum;
   circular_buffer_t buffer;
} ipod_receiver_t;

typedef enum {
   SEND_FF, SEND_55, SEND_LENGTH, SEND_MESSAGE, SEND_CHECKSUM, TX_FINISHED, TX_ERROR
} ipod_tx_state_t;

typedef struct {

   ipod_tx_state_t state;
   uint8_t length;
   uint8_t index;
   uint8_t checksum;
   uint8_t buffer[TX_BUFSIZ];
} ipod_transmitter_t;

ipod_receiver_t receiver;
ipod_transmitter_t transmitter;

///
///\brief Initialise USART
///
uint8_t usart_init(uint32_t baud)
{
   //cli();
   buffer_init(&(receiver.buffer));
   //receiver.state    = RX_FINISHED;

   //transmitter.state    = TX_FINISHED;
   //transmitter.index    = 0;
   //transmitter.length   = 0;
   //transmitter.checksum = 0;

   //UBRRH = 0;                               //19200 baud
   //UBRRL = 25;                            //19200 baud
   //UCSRA = 0;
   UCSRB = 0;
   UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);    //8N1

#if F_CPU < 2000000UL
   UCSRA = _BV(U2X);
   UBRRL = (F_CPU / (8UL * baud)) - 1;
#else
   UBRRL = (F_CPU / (16UL * baud)) - 1;
#endif

   UCSRB |= _BV(RXEN) | _BV(RXCIE);                  //enable receiver and receiver interrupts

   return 1;
}

///
///\brief empty receiver buffer
///
uint8_t usart_flush_buffer(void)
{
   return buffer_init(&(receiver.buffer));
}

///
///\brief Start transmission
///
uint8_t usart_write(void)
{
   if (transmitter.state == TX_FINISHED || transmitter.state == TX_ERROR)
   {
      if (receiver.state == RX_FINISHED || receiver.state == RX_ERROR || receiver.state == WAITING_FOR_FF)
      {
         transmitter.state = SEND_FF;      //reset transmitter state machine
         UCSRB |= _BV(TXEN) | _BV(UDRIE);   //enable transmitter, enable data register empty interrupts
         sei();
         //enable global interrupts
         return 1;
      }
   }
   return 0;
}

///
///\brief transmit single char
///
uint8_t usart_put_char(uint8_t data)
{
   while (!( UCSRA & _BV(UDRE)))
      ;
   UDR = data;
   return 1;
}

///
///\brief get and return char from receiver buffer
///
uint8_t usart_get_char(uint8_t *data)
{
   return buffer_get(data, &(receiver.buffer));
}

///
///\brief receiver interrupt
///
ISR(USART_RXC_vect)
{
   uint8_t tmp;
   tmp = UDR;
   if (!buffer_put(tmp, &(receiver.buffer)))
      UCSRB &= ~_BV(RXEN);   //disable receiver
   /*
    switch( receiver.state )
    {
    case WAITING_FOR_FF:
    if( tmp == 0xFF )
    receiver.state = WAITING_FOR_55;
    break;

    case WAITING_FOR_55:
    if( tmp == 0x55 )
    receiver.state = WAITING_FOR_LENGTH;
    else
    receiver.state = RX_ERROR;
    break;

    case WAITING_FOR_LENGTH:
    receiver.checksum = tmp;
    receiver.length   = tmp;
    receiver.tmplen   = tmp;
    receiver.state    = GETTING_MESSAGE;
    break;

    case GETTING_MESSAGE:
    receiver.checksum += tmp;
    if( !buffer_put( tmp, &(receiver.buffer) ) )
    receiver.state = RX_ERROR;
    if( !--receiver.tmplen )
    receiver.state = WAITING_FOR_CHECKSUM;
    break;

    case WAITING_FOR_CHECKSUM:
    //TODO: put checksum check here
    UCSRB &= ~_BV(RXEN);   //disable receiver
    receiver.state = RX_FINISHED;
    break;

    case RX_FINISHED:
    case RX_ERROR:
    UCSRB &= ~_BV(RXEN);   //disable receiver
    break;

    default:
    receiver.state = RX_ERROR;
    }
    */
}

///
///\brief transmitter interrupt
///
ISR(USART_UDRE_vect)
{
   /*   switch( transmitter.state )
    {
    case SEND_FF:                                          //send first header byte
    if( 2 < transmitter.length && transmitter.length < 11 )      //sanity check command length
    {
    transmitter.state = SEND_55;
    transmitter.index = 0;
    UDR = 0xFF;
    }
    else
    transmitter.state = TX_ERROR;
    break;

    case SEND_55:                                          //send second header byte
    transmitter.state = SEND_LENGTH;
    UDR = 0x55;
    break;

    case SEND_LENGTH:
    transmitter.checksum = transmitter.length;               //start checksum calc.
    transmitter.state = SEND_MESSAGE;
    UDR = transmitter.length;
    break;

    case SEND_MESSAGE:
    transmitter.checksum += transmitter.buffer[ transmitter.index ];      //calculate checksum
    UDR = transmitter.buffer[ transmitter.index ++ ];                  //send command byte
    if( transmitter.index == transmitter.length )                     //test for end of message
    transmitter.state = SEND_CHECKSUM;
    break;

    case SEND_CHECKSUM:
    transmitter.checksum = ( transmitter.checksum ^ 0xff ) + 1;
    transmitter.state = TX_FINISHED;
    UDR = transmitter.checksum;
    PORTA = transmitter.checksum;
    break;

    case TX_FINISHED:
    UCSRB &= ~( _BV(UDRIE) | _BV(TXEN) );               //disable transmitter
    receiver.state = WAITING_FOR_FF;                  //initialise receiver state machine
    UCSRB |= _BV(RXEN) | _BV(RXCIE);                  //enable receiver and receiver interrupts
    break;

    case TX_ERROR:
    UCSRB &= ~( _BV(UDRIE) | _BV(TXEN) );   //disable transmitter, effective when reg. empty
    break;

    default:
    transmitter.state = TX_ERROR;
    }
    */
}
