/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file      iiccharlcd.c
///
///\brief      HD44870 type LCD driver via i2c port PCF8574
///
///\verbatim
/// 4 bit interface, 1x PCF8574
/// ---------------------------
/// HOST I2C PORT -> PCF8574 -> LCD DATA LINES (D7-D4)
///                          -> LCD CONTROL LINES (E2,E,RW,RS)
///
/// 8 bit interface, 2x PCF8574
/// ---------------------------
/// HOST I2C PORT -> PCF8574 -> LCD DATA LINES (D7-D0)
///               -> PCF8574 -> LCD CONTROL LINES (E2,E,RW,RS)
///
/// Typical LCD display pin outs
///
/// |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 |
/// ---------------------------------------------------------------------------------
/// | Vss| Vdd| Vee| RS | R/W| E  | D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | A  | K  |
/// ---------------------------------------------------------------------------------
/// | D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | E1 | R/W| RS | Vee| Vss| Vdd| E2 | N/C|
/// ---------------------------------------------------------------------------------
///
///\endverbatim
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version   3.4 27/03/2011
///       - added lcd_printf() stdarg version of lcd_print()
///       - changed order of arguments for lcd_print and lcd_print_centered
///
///\version      3.3 12/10/2010
///            - added LCD width and height to lcd_open
///
///\version      3.2 10/10/2010
///            - formatted for doxygen
///
///\version      3.1 10/09/2007
///            - rewrite lcd_ioctl to use lcd_write
///            - introduced lcdInfo struct to hold
///              LCD control data such as mask, enable
///              and rs.
///            - Introduced IIC_LCD_CTRL_PORT and
///              IIC_LCD_DATA_PORT which should make it easy
///              to adapt for 8 bit ( 2 x PCF8574 ) or
///              4 bit ( 1 x PCF8574 ).
///
///\version      3.0 23/08/2007
///            - rewrite for 1 x PCF8574 and LCD in 4 bit mode
///            - change function names from C++ to C style
///            - added lcd_set_CG_RAM_P() which takes CGRAM
///              bit patterns from AVR program space.
///            - introduced BUSY FLAG read.
///
///\version      2.1 03/05/2005
///            - tweaked for PIC16F62X -> 2 x I2C port
///
///\version      2.0 24/10/2004
///            - ported to EVC (68HC11 -> 2 x I2C ports)
///            - removed serial, replaced with I2C.
///
///\version      1.2 04/03/2003
///            - Added serial LCD functionality
///
///\version      1.1   30/08/2002
///            - Removed 'line + offset' argument from
///              lcdPrint function, replaced with
///              single 'position'.
///            - Renamed functions to be more like
///              standard file IO.
///            - Replaced unsigned char with char
///              defined in my68hc11.h.
///            - Added lcdScroll();
///
///\version      1.0 14/05/2002
///            - Initial Release.
///
///   Copyright (C) 2002 - 2010 David McKelvie
///
/////////////////////////////////////////////////////////
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include "iic.h"
#include "pcf8574.h"
#include "iiccharlcd.h"

static uint8_t lcd_read_AC_BF(void);
static uint8_t lcd_read_AC(void);
static uint8_t lcd_read_BF(void);

static lcd_settings_t lcd;

/////////////////////////////////////////////////////////
///
///\brief      Initialises LCD display according to Hitachi
///            recommendations for software initialisation
///            of HD44870 LCD controllers.
///
///\param      type interface type
///\arg \c      LCD_8_BIT_BIG
///\arg \c      LCD_4_BIT_BIG
///\arg \c      LCD_8_BIT_SML
///\arg \c      LCD_4_BIT_SML
///\param      width width of LCD
///\param      height height of LCD
///
/////////////////////////////////////////////////////////
uint8_t lcd_open(uint8_t type, uint8_t width, uint8_t height)
{
   lcd.width = width;
   lcd.height = height;
   lcd.doubleW = 0;               // single write to LCD data lines

   lcd_switch_display(LCD_DISPLAY_ONE);

   lcd_ioctl(LCD_8_BIT);                                 // function set
   lcd_ioctl(LCD_8_BIT);                                 // function set
   lcd_ioctl(LCD_8_BIT);                                 // function set

   if ((type == LCD_4_BIT_SML) || (type == LCD_4_BIT_BIG))
   {
      lcd_ioctl(LCD_4_BIT);                              // function set
      lcd.doubleW = 1;
      lcd_ioctl(LCD_4_BIT | LCD_2_LINE);                  // function set
   }
   else
      lcd_ioctl(LCD_8_BIT | LCD_2_LINE);                  // function set

   lcd_ioctl(LCD_DISPLAY_OFF);                           // display off
   lcd_ioctl(LCD_CLEAR_DISPLAY);                           // clear display
   lcd_ioctl(LCD_INCREMENT_DD_RAM);                        // entry mode set
   lcd_ioctl(LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINK_OFF);   // display on

   // initialise second half of big display
   if ((type == LCD_8_BIT_BIG) || (type == LCD_4_BIT_BIG))
   {
      lcd.doubleW = 0;

      lcd_switch_display(LCD_DISPLAY_TWO);

      lcd_ioctl(LCD_8_BIT);                                 // function set
      lcd_ioctl(LCD_8_BIT);                                 // function set
      lcd_ioctl(LCD_8_BIT);                                 // function set

      if ((type == LCD_4_BIT_BIG))
      {
         lcd_ioctl(LCD_4_BIT);                              // function set
         lcd.doubleW = 1;
         lcd_ioctl(LCD_4_BIT | LCD_2_LINE);                  // function set
      }
      else
         lcd_ioctl(LCD_8_BIT | LCD_2_LINE);                  //function set

      lcd_ioctl(LCD_DISPLAY_OFF);                           // display off
      lcd_ioctl(LCD_CLEAR_DISPLAY);                           // clear display
      lcd_ioctl(LCD_INCREMENT_DD_RAM);                        // entry mode set
      lcd_ioctl(LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINK_OFF);   // display on
      lcd_switch_display(LCD_DISPLAY_ONE);
   }

   return 1;
}

/////////////////////////////////////////////////////////
///
///\brief      switch between LCD displays
///
///\details      This enables the control of 40 x 4 displays,
///            the top half is LCD_DISPLAY_ONE, the bottom
///            half is LCD_DISPLAY_TWO.
///
///\param      display display to switch to
///
/////////////////////////////////////////////////////////
uint8_t lcd_switch_display(uint8_t display)
{
   if (display == LCD_DISPLAY_TWO)
   {
      lcd.mask = IIC_LCD_CONTROL_MASK_2;
      lcd.enable = _BV(LCD_E2);
   }
   else
   {
      lcd.mask = IIC_LCD_CONTROL_MASK_1;
      lcd.enable = _BV(LCD_E);
   }

   return 1;
}

/////////////////////////////////////////////////////////
///
///\brief      writes command byte to LCD Instruction
///            Register by setting state of RS to 0,
///            before writing to LCD data lines.
///
///\param      command command to write to LCD
///
/////////////////////////////////////////////////////////
uint8_t lcd_ioctl(const uint8_t command)
{
   lcd.rs = 0;
   lcd_write(command);
   lcd.rs = _BV(LCD_RS);

   return 1;
}

/////////////////////////////////////////////////////////
///
///\brief      write single byte to LCD.
///
///\param      data data to write to LCD
///
//////////////////////////////////////////////////////////
uint8_t lcd_write(uint8_t data)
{
   uint8_t timeout = 0xFF;

   iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, lcd.rs);            //RS=rs, E=0, R/W=0
   iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, lcd.rs | lcd.enable);   //RS=rs, E=1, R/W=0
   iic_port_write(IIC_LCD_DATA_PORT, IIC_LCD_DATA_MASK, data);      //D0-7 = data
   iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, lcd.rs);            //RS=rs, E=0, R/W=0

   if (lcd.doubleW)
   {
      iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, lcd.rs | lcd.enable);   //RS=rs, E=1, R/W=0
      iic_port_write(IIC_LCD_DATA_PORT, IIC_LCD_DATA_MASK, data << 4);   //D4-7 = data
      iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, lcd.rs);            //RS=rs, E=0, R/W=0
   }

   while (lcd_read_BF() && --timeout)
      ;

   if (!timeout)
      return 0;
   return 1;
}

/////////////////////////////////////////////////////////
///
///\brief      Read LCD Address Counter (AC) and Busy Flag (BF)
///
///\note      when RS=0 and R/W=1 the Address counter is output
///             to D0-D6 and the Busy Flag is output to D7
///
///\return      combined address counter and busy flag
///
//////////////////////////////////////////////////////////
static uint8_t lcd_read_AC_BF(void)
{
   uint8_t tmp, reg;

   // set D7 - D4 bits high before use as inputs in accordance with PCF8574 data sheet.
   //iic_port_write( IIC_LCD_DATA_PORT, IIC_LCD_DATA_MASK, 0xFF );         //D4-7 = data

   iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, _BV(LCD_RW));         // RS=0, E=0, R/W=1
   iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, _BV(LCD_RW) | lcd.enable);      // RS=0, E=1, R/W=1
   iic_port_read(IIC_LCD_DATA_PORT, &reg);                     // MSB = BUSY FLAG
   iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, _BV(LCD_RW));         // RS=0, E=0, R/W=1

   if (lcd.doubleW)
   {
      iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, _BV(LCD_RW) | lcd.enable);   // RS=0, E=1, R/W=1
      iic_port_read(IIC_LCD_DATA_PORT, &tmp);                  // read lower nibble
      iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, _BV(LCD_RW));      // RS=0, E=0, R/W=1
   }
   iic_port_write(IIC_LCD_CTRL_PORT, lcd.mask, 0);               // RS=0, E=0, R/W=0

   return reg;
}

/////////////////////////////////////////////////////////
///
///\brief      Read LCD Address Counter (AC)
///
///\return      contents of Address Counter
///
//////////////////////////////////////////////////////////
static uint8_t lcd_read_AC(void)
{
   uint8_t ac = lcd_read_AC_BF();
   return (ac & 0x7F);
}

/////////////////////////////////////////////////////////
///
///\brief      Read LCD BUSY FLAG
///
///\return      busy flag
///
//////////////////////////////////////////////////////////
static uint8_t lcd_read_BF(void)
{
   uint8_t bf = lcd_read_AC_BF();
   return (bf & _BV(LCD_BF));
}

/////////////////////////////////////////////////////////
///
///\brief      LCD print function
///
///\details      Send a string of characters to the LCD starting
///            at the DDRAM address given by \p position
///
///            Two possible values for \p position are
///            LCD_LINE_ONE and LCD_LINE_TWO.
///
///            Writes characters until the terminating
///            '\\0' is encountered or 8 bit index
///            overflows.
///
///\param      position start position to write \p string
///\param   string string to write to display
///
/////////////////////////////////////////////////////////
uint8_t lcd_print(uint8_t position, const char *string)
{
   uint8_t index;
   index = 0;

   lcd_ioctl(position);               //set DDRAM address
   while (*(string + index))         //write until 0x00...
   {
      lcd_write(*(string + index));
      if (++index == 0)               //...or 256
         break;
   }
   return index;
}

/////////////////////////////////////////////////////////
///
///\brief   LCD print function
///
///\details   Send a string of characters to the LCD starting
///       at the DDRAM address given by \p position
///
///       Two possible values for \p position are
///       LCD_LINE_ONE and LCD_LINE_TWO.
///
///       Writes characters until the terminating
///       '\\0' is encountered or 8 bit index
///       overflows.
///
///\param   position start position to write \p string
///\param   string format string to write to display
///
/////////////////////////////////////////////////////////
uint8_t lcd_printf(uint8_t position, const char *string, ...)
{
   char buffer[200];
   va_list ap;

   va_start(ap, string);
   vsprintf(buffer, string, ap);
   va_end(ap);

   return lcd_print(position, buffer);
}

/////////////////////////////////////////////////////////
///
///\brief      add on for lcdPrint, roughly centers
///            text on LCD. Uses LCD width and length
///            of string given as \p string to calculate
///            a position offset.
///
///\param      string string to write to display
///\param      position start position to write \p string
///
/////////////////////////////////////////////////////////
uint8_t lcd_print_centered(uint8_t position, char *string)
{
   uint8_t offset;
   offset = strlen(string);
   if (offset > lcd.width)
      offset = lcd.width;
   offset = (lcd.width - offset) >> 1;
   lcd_print(position + offset, string);
   return 1;
}

/////////////////////////////////////////////////////////
///
///\brief      writes spaces to a single line of the LCD.
///
///\param      line line to blank e.g., LCD_LINE_ONE
///
/////////////////////////////////////////////////////////
uint8_t lcd_blank_line(uint8_t line)
{
   uint8_t i;

   lcd_ioctl(line);
   for (i = 0; i < lcd.width; i++)
      lcd_write(0x20);

   return 1;
}

/////////////////////////////////////////////////////////
///
///\brief      Sets character generator pattern in CGRAM.
///
///\details      Saves and restores AC (Address Counter)
///
///\param      cgAddress address in CGRAM to place custom
///            pattern, 0 - 7.
///
///\param      bitPattern pointer to char array containing
///            bit pattern of custom character.
///
///\verbatim
///BIT PATTERN     EG          HEX
/// 76543210
/// ---XXXXX        XXXX        1E
/// ---XXXXX        X   X       11
/// ---XXXXX        X   X       11
/// ---XXXXX        XXXX        1E
/// ---XXXXX        X   X       11
/// ---XXXXX        X   X       11
/// ---XXXXX        XXXX        1E
///\endverbatim
///
///\code
/// char letter_B[] = { 0x1E, 0x11, 0x11, 0x1E, 0x11, 0x11, 0x1E };
/// lcd_set_CG_RAM( LCD_CG_RAM_ONE, letter_B );
/// lcd_write( LCD_CG_RAM_ONE );
///\endcode
///
///\note      Avoid the use of CGRAM address 0 when
///            using lcd_print() to output strings containing
///            CG characters as lcd_print() stops writing
///            to LCD when it encounters '\\0' in a string.
///            Use lcd_write() to output single character
///            in this case.
///
/////////////////////////////////////////////////////////
uint8_t lcd_set_CG_RAM(uint8_t cgAddress, uint8_t *bitPattern)
{
   uint8_t i, ac = lcd_read_AC();            // save DDRAM address
   lcd_ioctl(LCD_SET_CG_RAM | cgAddress);   // set CGRAM address
   for (i = 0; i < 8; i++)               // write pattern
      lcd_write(bitPattern[(int) i]);
   lcd_ioctl(ac);                     // restore DDRAM address

   return 1;
}

/////////////////////////////////////////////////////////
///
///\brief      same as lcd_set_CG_RAM(), but takes string from
///            program space of AVR.
///
///\param      cgAddress address in CGRAM to place custom
///            pattern, 0 - 7.
///
///\param      bitPattern pointer to char array containing
///            bit pattern of custom character.
///
/////////////////////////////////////////////////////////
uint8_t lcd_set_CG_RAM_P(uint8_t cgAddress, const char * bitPattern)
{
   uint8_t i, ac = lcd_read_AC();               // save DDRAM address
   lcd_ioctl(LCD_SET_CG_RAM | cgAddress);       // set CGRAM address
   for(i = 0; i < 8; i ++ )                     // write pattern
      lcd_write(pgm_read_byte( bitPattern + i ));
   lcd_ioctl(ac);                               // restore DDRAM address

   return 1;
}
