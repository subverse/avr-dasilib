/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file    iic.c
///
///\brief    Driver for Atmel AVR Two Wire Serial Interface Module.
///
///\author    <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version    2.2 13/10/2010
///        - extracted port and pot functions to ds1807.c and pcf8574.c
///
///\version    2.1 10/10/2010
///        - formatted for doxygen
///
///\version    2.0 25/08/2007
///        - change function names from C++ to C style
///
///\version    1.0 10/08/2004
///        - initial release
///
///  Copyright (C) 2005 David McKelvie
///
/////////////////////////////////////////////////////////
#include<stdint.h>
#include<avr/io.h>
#include"iic.h"

///////////////////////////////////////////////////////////////
///
///\brief    Initialise atmega TWI (I2C) driver hardware
///
///\todo    calculate I2C bit rate from F_OSC
///
///////////////////////////////////////////////////////////////
uint8_t iic_init(void)
{
   TWBR = 80;                  // IIC bit rate
   //DDRC   |= _BV( PC0 ) | _BV( PC1 );      // make SCA and SCL outputs, the rest inputs
   //PORTC  &= ~( _BV( PC0 ) | _BV( PC1 ) );    // enable PORTC pullups

   return 1;
}

///////////////////////////////////////////////////////////////
///
///\brief    Write data to the I2C bus.
///
///\param    address   address of I2C device
///\param    offset    second byte written after address, can
///              be offset or command word.
///\param    buffer    pointer to uint8_t array containing
///              data to be written to bus.
///\param    length    number of elements from buffer to be
///              written to bus
///
///\return    number of elements from buffer written to bus
///
///\warning    It is the responsibility of the calling function to
///        ensure that the given length is not beyond the memory
///        reserved for the buffer, or you don't care about
///        writing random crap to the bus.
///
///\warning    writing process does not escape or terminate on 0
///
///////////////////////////////////////////////////////////////
uint8_t iic_write(uint8_t address, uint8_t offset, uint8_t * buffer, uint8_t length)
{
   uint8_t i;
   iic_start();
   iic_write_byte(address & IIC_WRITE);        //send device address and "write"
   iic_write_byte(offset);              //send word address

   for (i = 0; i < length; i++)
      if (!iic_write_byte(buffer[i]))
         break;

   iic_stop();
   return i;
}

///////////////////////////////////////////////////////////////
///
///\brief    Read data from the I2C bus.
///
///\param    address   address of I2C device
///\param    offset    second byte written after address, can
///              be offset or command word.
///\param    buffer    pointer to uint8_t array that will hold
///              data from I2C device.
///\param    length    number of elements to read from I2C device
///
///
///\return    1 on success, 0 on failure
///
///\warning    reading process does not escape or terminate on 0
///
///\warning    It is the responsibility of the calling function to
///        ensure that the given length is not beyond the memory
///        reserved for the buffer
///
///////////////////////////////////////////////////////////////
uint8_t iic_read(uint8_t address, uint8_t offset, uint8_t *buffer, uint8_t length)
{
   uint8_t i;

   if (length--)                    //catch 0 length and adjust
   {
      iic_start();
      iic_write_byte(address & IIC_WRITE);      //send device address and "write"
      iic_write_byte(offset);              //send word address
      iic_start();                    //restart
      iic_write_byte(address | IIC_READ);        //send device address and "read"

      for (i = 0; i < length; i++)
         if (iic_read_byte(buffer + i, ACK) != 0x50)  //read data, ACK returned?
         {
            iic_stop();
            return 0;
         }

      iic_read_byte(buffer + length, NACK);      //read last byte return NACK
      iic_stop();
      return 1;
   }
   return 0;
}

///////////////////////////////////////////////////////////////
///
///\brief  Write one single byte to the I2C bus.
///
///\param  data byte to be written to bus.
///
///\return  TWSR & MT_DATA_ACK, (non-zero on success)
///
///////////////////////////////////////////////////////////////
uint8_t iic_write_byte(uint8_t data)
{
   TWDR = data;                    //write data to transmitter register
   TWCR = _BV(TWINT) | _BV(TWEN);            //enable transmitter, clear interrupt flag
   while (!( TWCR & _BV(TWINT)))
      ;          //wait for data to leave data register
   return ( TWSR & MT_DATA_ACK);            //return non-zero on success
}

/////////////////////////////////////////////////////////////////
///
///\brief  Read one single byte from the I2C bus.
///
///\param  data pointer to uint8_t to hold received data
///\param  ack  return ACK to bus if non zero
///        return NACK to bus if zero
///
///\return  TWSR & 0xF8, non-zero, test for;
///      MR_DATA_ACK    byte sent ACK returned
///      MR_DATA_NACK  byte sent NACK returned
///
/////////////////////////////////////////////////////////////////
uint8_t iic_read_byte(uint8_t *data, uint8_t ack)
{
   if (ack)
      TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA);    //data byte will be read, ACK will be returned
   else
      TWCR = _BV(TWINT) | _BV(TWEN);          //data byte will be read, NACK will be returned

   while (!( TWCR & _BV(TWINT)))
      ;          //wait for data reception
   *data = TWDR;                    //save data
   return ( TWSR & 0xF8);
}

/////////////////////////////////////////////////////////////////
///
///\brief  Create I2C start condition
///
/////////////////////////////////////////////////////////////////
uint8_t iic_start(void)
{
   TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTA);      //generate start condition
   while (!( TWCR & _BV(TWINT)))
      ;          //wait for start condition
   return 1;
}

/////////////////////////////////////////////////////////////////
///
///\brief  Create I2C stop condition
///
/////////////////////////////////////////////////////////////////
uint8_t iic_stop(void)
{
   TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);    //generate stop condition
   return 1;
}
