/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\file gauge.c
///
///\brief LED bar gauge, fuel gauge type. LEDS connected to PCF8574
///
#include <stdint.h>
#include "iic.h"
#include "pcf8574.h"
#include "gauge.h"

///
///\brief   write to gauge
///
///\details Gauge range 0 - 119. Anything over this will light all LEDs
///
///\param   gauge iic address of PCF8574 to write to
///\param   value value to write
///
uint8_t gauge_write(uint8_t gauge, uint8_t value)
{
   uint8_t data;
   if (value > 119)
      data = 0xFF;
   else
   {
      value /= 15;
      value++;
      data = 1 << value;
   }
   iic_port_write(gauge, 0, data);
   return 1;
}
