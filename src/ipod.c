/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\file      ipod.c
///
///\brief      Advanced iPod Remote (AiR) using iPod Accessory Protocol
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\details      "The Apple Accessory Protocol is used for communication between the iPod
///           and serially connected accessories (such as the remote, iTalk etc).
///             The protocol was introduced with the 3rd generation iPods, and at present
///             it is assumed is also compatible with the 4th generation iPods and mini iPods."
///
///             http://www.ipodlinux.org/Apple_Accessory_Protocol
///
///\verbatim
///List of known Commands:
///IPOD_SWITCH_MODE_AIR           switch ipod into AiR Mode
///IPOD_GET_TYPE                  get ipod type, loads ipod.size and ipod.type (generation and size?)
///IPOD_GET_NAME                  get ipod name, loads name into ipod.name string
///IPOD_GET_TIME_STATUS           get ipod status and track time (total and elapsed)
///IPOD_PRESELECT_MAIN            preselect main playlist
///IPOD_GET_CURRENT_POSITION      get position in current playlist, loads position into song.current
///IPOD_GET_SHUFFLEMODE           get shufflemode, loads shufflemode into ipod.shufflemode
///IPOD_GET_REPEATMODE            get repeatmode, loads repeatmode into ipod.repeatmode
///IPOD_COUNT_SONGS               get count of songs in current playlist, loads count into song.total
///IPOD_GET_COUNT_OF_PLAYLISTS    get count of playlists, loads count into playlist.total
///IPOD_GET_COUNT_OF_ARTISTS      not implemented
///IPOD_GET_COUNT_OF_ALBUMS       not implemented
///IPOD_GET_COUNT_OF_GENRES       not implemented
///IPOD_GET_COUNT_OF_SONGS        get count of songs on device, loads count into totalSongs
///IPOD_GET_COUNT_OF_COMPOSERS    not implemented
///IPOD_SET_POLLINGMODE           not implemented
///IPOD_AIR_PLAYBACK              execute AiR playback command, executes command stored in ipod.airCommand
///IPOD_SET_SHUFFLEMODE           set shufflemode, uses ipod.shufflemode
///IPOD_SET_REPEATMODE            set repeatmode, uses ipod.repeatmode
///IPOD_GET_TITLE_OF              get title of current track, loads title into ipod.title string
///IPOD_GET_ARTIST_OF             get artist of current track, loads artist into ipod.artist string
///IPOD_GET_ALBUM_OF              get album of current track, loads album into ipod.album string
///IPOD_EXECUTE_PLAYLIST          do it baby
///IPOD_JUMP_TO                   not implemented
///IPOD_PRESELECT_X_NUMBER_N      not implemented
///IPOD_GET_NAMES_OF_X            not implemented
///IPOD_PRESELECT_PLAYLIST        preselect playlist given by playlist.current
///\endverbatim
///
/// IPOD DETECTION MECHANISM
///   - connect pin 15 of dock connector to input pin of micro
///   - connect pin 16 of dock to GND
///   - pull up micro input pin
///   - pins 15 and 16 are connected together in the ipod
///   - pin 15 will be pulled down when ipod is in dock
///
///   PD3 is external interrupt request 1 (INT1), this is used in
///   main to wake the CPU up on iPod insertion.
///
///\version      1.1 10/10/2010
///            - formatted for doxygen
///            - converted to C style function names
///
///\version      1.0 10/11/2006
///
///   Copyright (C) 2005 David McKelvie
///
#include <avr/pgmspace.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include "buffer.h"
#include "iiccharlcd.h"
#include "ipodsci.h"
#include "ipod.h"

static uint8_t ipod_switch_mode_air(ipod_t *);
static uint8_t ipod_get_string(char *);
static uint8_t ipod_in_dock(ipod_t *);
static uint8_t ipod_send_command(uint8_t);

static uint8_t ipod_get_type(ipod_t *);
static uint8_t ipod_get_name(ipod_t *);
static uint8_t ipod_get_time_status(ipod_t *);
static uint8_t ipod_preselect_main(ipod_t *);
static uint8_t ipod_get_current_position(ipod_t *);
static uint8_t ipod_get_shuffle_mode(ipod_t *);
static uint8_t ipod_get_repeat_mode(ipod_t *);
static uint8_t ipod_count_songs(ipod_t *);

static uint8_t ipod_get_count_of_playlists(ipod_t *);
static uint8_t ipod_get_count_of_artists(ipod_t *);
static uint8_t ipod_get_count_of_albums(ipod_t *);
static uint8_t ipod_get_count_of_genres(ipod_t *);
static uint8_t ipod_get_count_of_songs(ipod_t *);
static uint8_t ipod_get_count_of_composers(ipod_t *);
static uint8_t ipod_set_polling_mode(ipod_t *);
static uint8_t ipod_AiR_playback(ipod_t *);
static uint8_t ipod_set_shuffle_mode(ipod_t *);
static uint8_t ipod_set_repeat_mode(ipod_t *);

static uint8_t ipod_get_title_of(ipod_t *);
static uint8_t ipod_get_artist_of(ipod_t *);
static uint8_t ipod_get_album_of(ipod_t *);
static uint8_t ipod_execute_playlist(ipod_t *);
static uint8_t ipod_jump_to(ipod_t *);

static uint8_t ipod_preselect_x_number_n(ipod_t *);
static uint8_t ipod_get_names_of_x(ipod_t *);
static uint8_t ipod_preselect_playlist(ipod_t *);
static uint8_t ipod_inherent_command(uint8_t);
static uint8_t ipod_direct_command(uint8_t, uint8_t);
static uint8_t ipod_extended_command(uint8_t, uint16_t);

///\brief ipod_ioctl dispatch table
uint8_t (*ptrF[])(ipod_t *) =
{  ipod_switch_mode_air, ipod_get_type, ipod_get_name, ipod_get_time_status,
   ipod_preselect_main, ipod_get_current_position, ipod_get_shuffle_mode, ipod_get_repeat_mode,
   ipod_count_songs, ipod_get_count_of_playlists, ipod_get_count_of_artists, ipod_get_count_of_albums,
   ipod_get_count_of_genres, ipod_get_count_of_songs, ipod_get_count_of_composers, ipod_set_polling_mode,
   ipod_AiR_playback, ipod_set_shuffle_mode, ipod_set_repeat_mode, ipod_get_title_of, ipod_get_artist_of,
   ipod_get_album_of, ipod_execute_playlist, ipod_jump_to, ipod_preselect_x_number_n,
   ipod_get_names_of_x, ipod_preselect_playlist, ipod_in_dock };

extern ipod_transmitter_t transmitter;

///
///\brief Establish communications with iPod device.
///
///\details switches device into AiR mode and puts data
///         into ipod structure that may be of use,
///         such as:
///               ipod->name,
///               ipod->type,
///               ipod->size,
///               ipod->playlist.total
///               ipod->totalSongs
///               ipod->song.total
///               ipod->song.current
///               ipod->status
///
///\param ipod pointer to ipod structure
///
uint8_t ipod_open(ipod_t *ipod)
{
   uint8_t try = 5;
   //_delay_ms( 10 );
   ipod_sci_init();
   _delay_ms(200);
   while (try--)
      if (ipod_ioctl(ipod, IPOD_SWITCH_MODE_AIR))
      {
         _delay_ms(10);
         ipod_ioctl(ipod, IPOD_GET_TYPE);
         _delay_ms(10);
         ipod_ioctl(ipod, IPOD_GET_NAME);
         _delay_ms(10);
         ipod_ioctl(ipod, IPOD_GET_COUNT_OF_PLAYLISTS);      //get the number of available playlists
         _delay_ms(10);
         ipod_ioctl(ipod, IPOD_GET_COUNT_OF_SONGS);         //count total songs on device
         _delay_ms(10);
         ipod_ioctl(ipod, IPOD_COUNT_SONGS);            //count number of songs in current playlist
         _delay_ms(10);
         ipod_ioctl(ipod, IPOD_GET_CURRENT_POSITION);      //get current position in playlist
         if (ipod->totalSongs == ipod->song.total)   //are we in the main playlist?
            ipod->playlist.current = 0;               //I can't believe that there's no way to find this out
         else
            ipod->playlist.current = 1;
         ipod->airCommand = AIR_COMMAND_PLAY_PAUSE;      //set this to a known state
         return 1;
      }
   return 0;
}

///
///\brief   ipod accessor method
///
///\details   make appropriate modifications to ipod handle struct
///         call ipod_ioctl with proper command. ie: to execute an
///         ipod AiR remote command, load an AiR playback command
///         such as AIR_COMMAND_PLAY_PAUSE into the airCommand vaiable
///         of the ipod structure. Then call ipod_ioctl with the
///         IPOD_AIR_PLAYPACK command.
///
///\param ipod pointer to ipod structure
///
///\param command command to execute
///
uint8_t ipod_ioctl(ipod_t *ipod, uint8_t command)
{
   if (ipod_in_dock(ipod))
   {
      ipod_sci_flush_buffer();
      return ptrF[(int) command](ipod);   //array subscript is cast to int to avoid compiler warning
   }
   return 0;
}

///
///\brief Determine if an iPod is in the dock
///
///\return 1 if iPod in dock
///
static uint8_t ipod_in_dock(ipod_t *ipod)
{
   return bit_is_clear(IPOD_DETECT_PORT, IPOD_DETECT_PIN);
}

///
///\brief Switch iPod device into AiR mode
///
static uint8_t ipod_switch_mode_air(ipod_t *ipod)
{
   transmitter.buffer[0] = 0x00;      //mode switching mode
   transmitter.buffer[1] = 0x01;      //switch to...
   transmitter.buffer[2] = 0x04;      //...AiR mode
   transmitter.length = 3;

   if (ipod_in_dock(ipod))
   {
      if (ipod_sci_write())
      {
         _delay_ms(200);
         ipod_sci_flush_buffer();
         return 1;
      }
   }
   return 0;
}

///
///\ingroup ipod_command_inherent
///
///\brief Get iPod type
///
///\details loads into given ipod structure:
///               ipod->type
///               ipod->size
///
///\param ipod pointer to ipod structure
///
///\return 1 on success
///
static uint8_t ipod_get_type(ipod_t *ipod)
{
   if (ipod_inherent_command(GET_IPOD_TYPE))
   {
      ipod_sci_get_char(&(ipod->type));
      ipod_sci_get_char(&(ipod->size));
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_inherent
///
///\brief Get iPod name
///
///\details Put iPod name and place into ipod structure as:
///         ipod->name
///
static uint8_t ipod_get_name(ipod_t *ipod)
{
   if (ipod_inherent_command(GET_IPOD_NAME))
      return ipod_get_string(ipod->name);
   return 0;
}

///
///\ingroup ipod_command_inherent
///
///\brief Get track time and status.
///
///\details total and elapsed track time is placed into ipod sturcture as:
///         ipod->trackTime.total
///         ipod->trackTime.elapsed
///
///\details   which are 32 bit millisecond values.
///
///\details   status is placed into ipod structure as single byte:
///         ipod->status
///
///\details   This indicates if the ipod device is:
///         - STATUS_STOPPED,
///         - STATUS_PLAYING,
///         - STATUS_PAUSED.
///
static uint8_t ipod_get_time_status(ipod_t *ipod)
{
   uint8_t tmp;
   if (ipod_inherent_command(GET_TIME_STATUS))
   {
      ipod_sci_get_char(&tmp);
      ipod->trackTime.total = (uint32_t) tmp << 24;
      ipod_sci_get_char(&tmp);
      ipod->trackTime.total |= (uint32_t) tmp << 16;
      ipod_sci_get_char(&tmp);
      ipod->trackTime.total |= (uint32_t) tmp << 8;
      ipod_sci_get_char(&tmp);
      ipod->trackTime.total |= (uint32_t) tmp;
      ipod_sci_get_char(&tmp);
      ipod->trackTime.elapsed = (uint32_t) tmp << 24;
      ipod_sci_get_char(&tmp);
      ipod->trackTime.elapsed |= (uint32_t) tmp << 16;
      ipod_sci_get_char(&tmp);
      ipod->trackTime.elapsed |= (uint32_t) tmp << 8;
      ipod_sci_get_char(&tmp);
      ipod->trackTime.elapsed |= (uint32_t) tmp;
      ipod_sci_get_char(&(ipod->status));
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_inherent
///
///\brief Preselect main playlist.
///
///\details After the main playlist is selected it can be executed
///         with ipod_ioctl( &ipod, IPOD_EXECUTE_PLAYLIST );
///
static uint8_t ipod_preselect_main(ipod_t *ipod)
{
   return ipod_inherent_command(PRESELECT_MAIN_LIST);
}

///
///\ingroup ipod_command_inherent
///
///\brief Get current position in playlist
///
///\details place current position into ipod structure as two byte:
///         ipod::song::current
///
///\details This concatenates the four byte position returned
///         by the iPod, taking the two least significant bytes
///         as the current position.
///
static uint8_t ipod_get_current_position(ipod_t *ipod)
{
   uint8_t tmp;
   if (ipod_inherent_command(GET_CURRENT_POSITION))
   {
      ipod_sci_get_char(&tmp);         //get and discard MSB
      ipod_sci_get_char(&tmp);         //get and discard next byte
      ipod_sci_get_char(&tmp);
      ipod->song.current = tmp << 8;
      ipod_sci_get_char(&tmp);
      ipod->song.current |= tmp;
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_inherent
///
///\brief Get iPod shuffle mode.
///
///\details Place shuffle mode into ipod structure as:
///         ipod::shufflemode
///
///\details   shufflemode will be:
///         - SHUFFLEMODE_OFF
///         - SHUFFLEMODE_SONGS
///         - SHUFFLEMODE_ALBUMS
///
static uint8_t ipod_get_shuffle_mode(ipod_t *ipod)
{
   if (ipod_inherent_command(GET_SHUFFLEMODE))
   {
      ipod_sci_get_char(&(ipod->shufflemode));
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_inherent
///
///\brief Get iPod repeat mode.
///
///\details Place repeat mode into ipod structure as:
///         ipod::repeatmode
///
///\details repeatmode will be one of:
///\arg \c REPEAT_OFF
///\arg \c REPEAT_SONG
///\arg \c REPEAT_ALL
///
static uint8_t ipod_get_repeat_mode(ipod_t *ipod)
{
   if (ipod_inherent_command(GET_REPEATMODE))
   {
      ipod_sci_get_char(&(ipod->repeatmode));
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_inherent
///
///\brief Get count of songs in current playlist
///
///\details Place song count into ipod structure as:
///         ipod::song.total
///
///\note This counts only the songs in the current playlist
///      to get a count of all the songs on the ipod device
///      use ipod_ioctl( ipod, IPOD_GET_COUNT_OF_SONGS );
///
static uint8_t ipod_count_songs(ipod_t *ipod)
{
   uint8_t tmp;
   if (ipod_inherent_command(COUNT_SONGS))
   {
      ipod_sci_get_char(&tmp);
      ipod_sci_get_char(&tmp);
      ipod_sci_get_char(&tmp);
      ipod->song.total = tmp << 8;
      ipod_sci_get_char(&tmp);
      ipod->song.total |= tmp;
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_direct
///
///\brief Get count of playlists
///
///\details Place count into ipod structure as:
///         ipod->playlist.total
///
static uint8_t ipod_get_count_of_playlists(ipod_t *ipod)
{
   uint8_t tmp;
   if (ipod_direct_command(GET_COUNT_OF, TYPE_PLAYLIST))
   {
      ipod_sci_get_char(&tmp);
      ipod_sci_get_char(&tmp);
      ipod_sci_get_char(&tmp);
      ipod->playlist.total = tmp << 8;
      ipod_sci_get_char(&tmp);
      ipod->playlist.total |= tmp;
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_direct
///
///\brief Get count of Artists
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_get_count_of_artists(ipod_t *ipod)
{
   return 0;
}

///
///\ingroup ipod_command_direct
///
///\brief Get count of Albums
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_get_count_of_albums(ipod_t *ipod)
{
   return 0;
}

///
///\ingroup ipod_command_direct
///
///\brief Get count of Genres
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_get_count_of_genres(ipod_t *ipod)
{
   return 0;
}

///
///
///\ingroup ipod_command_direct
///
///\brief Get count of all songs on iPod
///
///\details Place song count into ipod structure as:
///         ipod->totalSongs
///
///\note   This counts all of the songs on the ipod device, to get
///       a count of only the songs in the current playlist
///       use ipod_ioctl( &ipod, IPOD_COUNT_SONGS );
///
static uint8_t ipod_get_count_of_songs(ipod_t *ipod)
{
   uint8_t tmp;
   if (ipod_direct_command(GET_COUNT_OF, TYPE_SONG))
   {
      ipod_sci_get_char(&tmp);
      ipod_sci_get_char(&tmp);
      ipod_sci_get_char(&tmp);
      ipod->totalSongs = tmp << 8;
      ipod_sci_get_char(&tmp);
      ipod->totalSongs |= tmp;
      return 1;
   }
   return 0;
}

///
///\ingroup ipod_command_direct
///
///\brief Get count of Composers
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_get_count_of_composers(ipod_t *ipod)
{
   return 0;
}

///
///\ingroup ipod_command_direct
///
///\brief Set polling mode
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_set_polling_mode(ipod_t *ipod)
{
   return 0;
}

///
///\ingroup ipod_command_direct
///
///\brief Execute ipod AiR playback command
///
///\details The Command to send should be set before invocation.
///         This is done by setting ipod.airCommand. This should
///         be set to one of:
///
///\arg \c AIR_COMMAND_PLAY_PAUSE
///\arg \c AIR_COMMAND_STOP
///\arg \c AIR_COMMAND_SKIP_FORWARD
///\arg \c AIR_COMMAND_SKIP_BACKWARD
///\arg \c AIR_COMMAND_FF
///\arg \c AIR_COMMAND_FB
///\arg \c AIR_COMMAND_STOP_FF_FB
///
static uint8_t ipod_AiR_playback(ipod_t *ipod)
{
   transmitter.buffer[0] = 0x04;
   transmitter.buffer[1] = 0x00;
   transmitter.buffer[2] = AIR_COMMAND;
   transmitter.buffer[3] = ipod->airCommand;
   transmitter.length = 4;
   while (!ipod_sci_write())
      ;
   _delay_ms(5);
   ipod_sci_flush_buffer();
   return 1;
}

///
///\ingroup ipod_command_direct
///
///\brief Set iPod shuffle mode.
///
///\details Sets shuffle mode to ipod.shufflemode
///
///\warning This is one of those commands where the "get" and "set" invocation
///         of ipod_ioctl use the same variable from the ipod structure
///
static uint8_t ipod_set_shuffle_mode(ipod_t *ipod)
{
   return ipod_direct_command(SET_SHUFFLEMODE, ipod->shufflemode);
}

///
///\ingroup ipod_command_direct
///
///\brief Set iPod repeat mode.
///
///\details Sets repeat mode to ipod.repeatemode
///
///\warning This is one of those commands where the "get" and "set" invocation
///         of ipod_ioctl use the same variable from the ipod structure
///
static uint8_t ipod_set_repeat_mode(ipod_t *ipod)
{
   return ipod_direct_command(SET_REPEATMODE, ipod->repeatmode);
}

///
///\ingroup ipod_command_extended
///
///\brief Get title of current song
///
///\details   Title is placed into ipod structure as ipod.title
///         which will be a null terminated string of length
///         IPOD_BUFSIZ or less.
///
static uint8_t ipod_get_title_of(ipod_t *ipod)
{
   if (ipod_extended_command(GET_TITLE_OF, ipod->song.current))
      return ipod_get_string(ipod->title);
   return 0;
}

///
///\ingroup ipod_command_extended
///
///\brief Get artist of current song
///
///\details   Artist is placed into ipod structure as ipod.artist
///         which will be a null terminated string of length
///         IPOD_BUFSIZ or less.
///
static uint8_t ipod_get_artist_of(ipod_t *ipod)
{
   if (ipod_extended_command(GET_ARTIST_OF, ipod->song.current))
      return ipod_get_string(ipod->artist);
   return 0;
}

///
///\ingroup ipod_command_extended
///
///\brief Get album of current song
///
///\details   Album is placed into ipod structure as ipod.album
///         which will be a null terminated string of length
///         IPOD_BUFSIZ or less.
///
static uint8_t ipod_get_album_of(ipod_t *ipod)
{
   if (ipod_extended_command(GET_ALBUM_OF, ipod->song.current))
      return ipod_get_string(ipod->album);
   return 0;
}

///
///\brief Get string from iPod
///
///\param string character array, must be IPOD_BUFSIZ length
///         will be loaded with string read from iPod
///
static uint8_t ipod_get_string(char *string)
{
   uint8_t i, tmp;
   i = 0;
   while (ipod_sci_get_char(&tmp))
   {
      if (i < IPOD_BUFSIZ)            //concatenate string
         *(string + i) = tmp;
      else
         break;
      i++;
   }
   *(string + i) = '\0';                  //terminate string
   ipod_sci_flush_buffer();
   return 1;
}

///
///\brief Execute the currently selected playlist.
///
static uint8_t ipod_execute_playlist(ipod_t *ipod)
{
   uint8_t tmp;
   transmitter.buffer[0] = 0x04;
   transmitter.buffer[1] = 0x00;
   transmitter.buffer[2] = EXECUTE_PLAYLIST;
   transmitter.buffer[3] = 0x00;
   transmitter.buffer[4] = 0x00;
   transmitter.buffer[5] = 0x00;
   transmitter.buffer[6] = 0x00;
   transmitter.length = 7;
   ipod_sci_write();
   while (!ipod_sci_get_char(&tmp))
      ;         //get and discard first character 0x04
   ipod_sci_flush_buffer();
   return 1;
}

///
///\brief Relative jump within current playlist
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_jump_to(ipod_t *ipod)
{
   return 0;
}

///
///\brief Preselect Type X, number N.
///
///\details Used to preselect items other than playlists e.g., select by artist or genre.
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_preselect_x_number_n(ipod_t *ipod)
{
   return 0;
}

///
///\brief Get names for range of items.
///
///\warning Not Implemented
///
///\todo Implement me
///
static uint8_t ipod_get_names_of_x(ipod_t *ipod)
{
   return 0;
}

///
///\brief Preselect playlist
///
///\details Selected playlist is given by ipod.playlist.current
///
///
static uint8_t ipod_preselect_playlist(ipod_t *ipod)
{
   uint8_t tmp;
   transmitter.buffer[0] = 0x04;
   transmitter.buffer[1] = 0x00;
   transmitter.buffer[2] = PRESELECT_X_NUMBER_N;
   transmitter.buffer[3] = TYPE_PLAYLIST;
   transmitter.buffer[4] = 0x00;
   transmitter.buffer[5] = 0x00;
   transmitter.buffer[6] = (uint8_t) (ipod->playlist.current >> 8);
   transmitter.buffer[7] = (uint8_t) ipod->playlist.current;
   transmitter.length = 8;
   ipod_sci_write();
   while (!ipod_sci_get_char(&tmp))
      ;         //get and discard first character 0x04
   ipod_sci_flush_buffer();
   return 1;
}

///
///\brief Send command to iPod
///
///\details   Start transmitter, wait for a reply, remove header from reply.
///
///\param command command to send
///
///\return 1 on success
///
static uint8_t ipod_send_command(uint8_t command)
{
   uint8_t tmp;
   unsigned int timeout;
   timeout = 0x000F;

   if (ipod_sci_write())
   {
      timeout = 0x00FF;
      while (!ipod_sci_get_char(&tmp))   //get and discard first character 0x04
      {
         _delay_ms(1);
         if (!timeout--)
            return 0;
      }
      ipod_sci_get_char(&tmp);            //get second character 0x00
      ipod_sci_get_char(&tmp);            //get third character command + 1
      if (++command == tmp)
         return 1;
      else
         return 0;
   }
   return 0;
}

///
///\brief Send inherent command to iPod
///
///\details Send iPod command from group of commands with no parameters.
///
///\param command command to send
///
///\return 1 on success
///
static uint8_t ipod_inherent_command(uint8_t command)
{
   transmitter.buffer[0] = 0x04;
   transmitter.buffer[1] = 0x00;
   transmitter.buffer[2] = command;
   transmitter.length = 3;
   return ipod_send_command(command);
}

///
///\brief Send direct command to iPod
///
///\details Send iPod command from group of commands with one byte parameter.
///
///\param command command to send
///
///\param param parameter to send
///
///\return 1 on success
///
static uint8_t ipod_direct_command(uint8_t command, uint8_t param)
{
   transmitter.buffer[0] = 0x04;
   transmitter.buffer[1] = 0x00;
   transmitter.buffer[2] = command;
   transmitter.buffer[3] = param;
   transmitter.length = 4;
   return ipod_send_command(command);
}

///
///\brief Send extended command to iPod
///
///\details Send iPod command from group of commands with four byte parameter.
///
///\param command command to send
///
///\param param parameter to send (lower two bytes)
///
///\return 1 on success
///
static uint8_t ipod_extended_command(uint8_t command, uint16_t param)
{
   transmitter.buffer[0] = 0x04;
   transmitter.buffer[1] = 0x00;
   transmitter.buffer[2] = command;
   transmitter.buffer[3] = 0x00;
   transmitter.buffer[4] = 0x00;
   transmitter.buffer[5] = (uint8_t) (param >> 8);
   transmitter.buffer[6] = (uint8_t) param;
   transmitter.length = 7;
   return ipod_send_command(command);
}

uint8_t ipod_set_flag(ipod_t * ipod)
{
   ipod_direct_command(0x0B, 0x01);
   ipod_sci_flush_buffer();

   return 1;
}

uint8_t ipod_read_flag(ipod_t * ipod)
{
   uint8_t tmp;
   ipod_inherent_command(0x09);
   ipod_sci_get_char(&tmp);
   return tmp;
}

