/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//////////////////////////////////////////////////////
///
///\file      bargraph.c
///
///\brief     Bar graph display for HD44870 type LCD.
///           An extension to LCD functions of iiccharlcd.c.
///
///\details   Initialise LCD using functions in iiccharlcd.c.
///
///           Call bar_init(); to load CG RAM
///           with bar characters
///
///           Call bar_write( n, s ), where
///           n is the number of bars on the display to
///           be 'blacked out', and s the start position
///           of the bar graph.
///
///\author    <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version   2.1 10/10/2010
///           - formatted for doxygen
///
///\version   2.0 23/08/2007
///           - Introduced V3.0 LCD functions
///           - converted from C++ to C function name style
///           - made CG RAM address 0 valid bar character ' '
///
///\version   1.2 31/08/2002
///           - Introduced V1.1 LCD functions
///
///\version   1.1  19/08/2002
///           - third method of setting bar graph
///           - characters introduced.
///
///\version   1.0  14/05/2002
///           - initial release
///
//////////////////////////////////////////////////////
#include <avr/pgmspace.h>
#include <stdint.h>
#include "iiccharlcd.h"
#include "bargraph.h"

static void bar_load_CG_RAM(void);

static bargraph_settings_t bargraph;
static uint8_t init = 0;

////////////////////////////////////////////////////////
///
///\brief    Initialise bar graph
///
///\param    width width of bar graph in LCD characters
///\param    max number that will completely fill bar graph
///
////////////////////////////////////////////////////////
uint8_t bar_init(uint8_t width, uint8_t max)
{
   if (max > (width * LCD_CHAR_WIDTH))
   {
      bar_load_CG_RAM();
      bargraph.width = width;
      bargraph.max = max;
      bargraph.bar_weight = (max / (width * LCD_CHAR_WIDTH));
      bargraph.char_weight = bargraph.bar_weight * LCD_CHAR_WIDTH;
      init = 1;
      return 1;
   }
   return 0;
}

////////////////////////////////////////////////////////
///
///\brief    loads bar graph characters into CG RAM
///
///\verbatim
///CG RAM Addr: 0       1       2       3       4       5
///             43210   43210   43210   43210   43210   43210
///
///                     X       XX      XXX     XXXX    XXXXX
///                     X       XX      XXX     XXXX    XXXXX
///                     X       XX      XXX     XXXX    XXXXX
///                     X       XX      XXX     XXXX    XXXXX
///                     X       XX      XXX     XXXX    XXXXX
///                     X       XX      XXX     XXXX    XXXXX
///                     X       XX      XXX     XXXX    XXXXX
///
///             0x00    0x10    0x18    0x1C    0x1E    0x1F
///\endverbatim
////////////////////////////////////////////////////////
static void bar_load_CG_RAM(void)
{
   uint8_t i;
   lcd_ioctl( LCD_SET_CG_RAM);  // set CG ram address
   for (i = 0; i < 8; i++)    // write BAR_NONE pattern
      lcd_write(0x00);
   for (i = 0; i < 7; i++)    // write BAR_ONE pattern
      lcd_write(0x10);
   lcd_write(0x00);
   for (i = 0; i < 7; i++)    // write BAR_TWO pattern
      lcd_write(0x18);
   lcd_write(0x00);
   for (i = 0; i < 7; i++)    // write BAR_THREE pattern
      lcd_write(0x1C);
   lcd_write(0x00);
   for (i = 0; i < 7; i++)    // write BAR_FOUR pattern
      lcd_write(0x1E);
   lcd_write(0x00);
   for (i = 0; i < 7; i++)    // write BAR_FIVE pattern
      lcd_write(0x1F);
   lcd_write(0x00);
}

/////////////////////////////////////////////////////////
///
///\brief    fills bar graph space in LCD memory with
///        characters from CG RAM to make bar graph.
///
///\details    call bar_init() first to ensure
///        correct contents of CG RAM.
///        Don't change CG RAM contents.
///
///\param       value 0 - 80 representing 80 bars on a 16 char display.
///\param       position start position of bar graph
///
///\return      1 on success
///
/////////////////////////////////////////////////////////
uint8_t bar_write(uint8_t value, uint8_t position)
{
   uint8_t i, j;

   if (init == 0)
      return 0;

   lcd_ioctl(position);      // set LCD AC to start of bar graph

   if (value > bargraph.max)  // out of range?
   {
      for (i = 1; i < bargraph.width; i++)
         lcd_write(BAR_FIVE);
      lcd_write('E');
      return 0;
   }

   j = 0;
   while (value > bargraph.char_weight)  // fill solid characters
   {
      lcd_write( BAR_FIVE);
      value -= bargraph.char_weight;
      j++;
   }
   i = 0;
   while (value > bargraph.bar_weight)
   {
      value -= bargraph.bar_weight;
      i++;
   }
   lcd_write(i);          // set final bar graph character

   while (j++ < bargraph.bar_weight)  // fill remaining space with ...
      lcd_write(BAR_NONE);    // ... blank character.

   return 1;

}    //end bar_write()
