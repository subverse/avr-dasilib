///
///\file   button.c
///
///\brief   driver for push buttons connected via PCF8574
///
///\author   <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version   1.2 13/10/2010
///         - removed mode setting, now a timeout of 0 means blocking
///
///\version   1.1 10/10/2010
///         - formatted for doxygen
///
///\version   1.0
///
#include <stdint.h>
#include "iic.h"
#include "pcf8574.h"
#include "button.h"

/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\brief   get button state
///
///\param   timeout count to wait for button press, 0 for no timeout (blocking)
///
///\return   button state
///
uint8_t button_get(uint16_t timeout)
{
   uint8_t buttons = BUTTON_TIMEOUT;
   if (timeout == 0)                           // In blocking mode, trap...
   {
      do                                    // ..."key already pressed"
      {
         iic_port_read(IIC_BUTTONS, &buttons);
      } while (buttons != RAW_BUTTON_NONE);
   }
   else
   {
      while ((timeout--) && buttons != RAW_BUTTON_NONE)
      {
         iic_port_read(IIC_BUTTONS, &buttons);         // get button state
         if (buttons != RAW_BUTTON_NONE)            // key is pressed?
         {
            iic_port_read(IIC_BUTTONS, &buttons);      // get debounced state
         }
      }
   }
   return buttons;
}
