/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file      buffer.c
///
///\brief      Circular buffer
///
///\details      Declare a variable of type "circular_buffer_t",
///
///            call buffer_init() with a pointer
///            to the circular_buffer_t, then call:
///               buffer_put()
///               buffer_get()
///            which return 1 if successful or
///            0 on failure.
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version      1.0
///
/////////////////////////////////////////////////////////
#include <stdint.h>
#include "buffer.h"

//////////////////////////////////////////////////////////
///
///\brief   initialise buffer
///
///\param   ptrBuffer pointer to a circular_buffer_t
///
//////////////////////////////////////////////////////////
uint8_t buffer_init(circular_buffer_t *ptrBuffer)
{
   ptrBuffer->head = 0;         //set indexes
   ptrBuffer->tail = 0;
   return 1;
}

//////////////////////////////////////////////////////////
///
///\brief   take one character from buffer, placing
///         it at the location pointed to by ptrChar
///
///\param   ptrChar pointer to load data into
///\param   ptrBuffer pointer to circular_buffer_t
///
//////////////////////////////////////////////////////////
uint8_t buffer_get(uint8_t *ptrChar, circular_buffer_t *ptrBuffer)
{
   if (ptrBuffer->head == ptrBuffer->tail)         // is buffer empty
      return 0;
   *ptrChar = ptrBuffer->buffer[ptrBuffer->head++];   // get char
   if (ptrBuffer->head == BUFSIZ)                  // circulate buffer
      ptrBuffer->head = 0;
   return 1;
}

//////////////////////////////////////////////////////////
///
///\brief   place data in buffer
///
///\param   data data to place in buffer
///\param   ptrBuffer pointer to circular_buffer_t
///
///\return   data from buffer
///
//////////////////////////////////////////////////////////
uint8_t buffer_put(uint8_t data, circular_buffer_t *ptrBuffer)
{
   if (ptrBuffer->head == ((ptrBuffer->tail + 1) % BUFSIZ))      //is buffer full
      return 0;
   ptrBuffer->buffer[ptrBuffer->tail] = data;      // put char
   if (++ptrBuffer->tail == BUFSIZ)               // circulate  buffer
      ptrBuffer->tail = 0;
   return 1;
}
