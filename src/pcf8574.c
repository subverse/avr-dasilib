/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file      pcf8574.c
///
///\brief      Driver for PCF8574 i2c 8 bit IO port.
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version      2.2 13/10/2010
///            - extracted port functions from iic.c
///
///\version      2.1 10/10/2010
///            - formatted for doxygen
///
///\version      2.0 25/08/2007
///            - change function names from C++ to C style
///
///\version      1.0 10/08/2004
///            - initial release
///
///   Copyright (C) 2005 David McKelvie
///
/////////////////////////////////////////////////////////
#include <stdint.h>
#include <avr/io.h>
#include "iic.h"
#include "pcf8574.h"

///\brief   Maintain i2c IO port states (PCF8574) maximum of 8
///\details   This keeps track of data written to port, does not tell you pin state.
///         Get pin state by doing an iic_port_read().
static uint8_t iicPortState[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

//uint8_t iicPinState[8];

///////////////////////////////////////////////////////////////
///
///\brief      write data to I2C IO port (PCF8574) while
///            maintaining state of masked bits.
///
///\details      Gives the ability to ignore but not affect the current state of some
///            IO lines while writing to others.
///            writes to iicPortState
///
///\param      address    address of I2C device as defined in iic.h
///\param      mask       bit mask, does not affect port state when masked bit is 1
///\param      data      byte to write to i2c device
///
///////////////////////////////////////////////////////////////
uint8_t iic_port_write(uint8_t address, uint8_t mask, uint8_t data)
{
   uint8_t index;
   index = address >> 1;                     //convert I2C address to array index
   data &= ~mask;                           // mask data not to be changed
   iicPortState[index] &= mask;               //apply data mask
   iicPortState[index] |= data;               //set/clear bits to be changed
   iic_start();
   iic_write_byte((PCF8574 | address) & IIC_WRITE);
   iic_write_byte(iicPortState[index]);
   iic_stop();
   return 1;
}

///////////////////////////////////////////////////////////////
///
///\brief      Read byte from I2C IO port (PCF8574).
///
///\param      address Address of i2c device, see iic.h
///\param      data Pointer to uint8_t, where data will be stored
///
///\details      writes to location given by \p data
///
///\note      Does not write to iicPortState array, should it?
///            it could write to iicPinState
///
///////////////////////////////////////////////////////////////
uint8_t iic_port_read(uint8_t address, uint8_t *data)
{
   iic_start();
   iic_write_byte(PCF8574 | address | IIC_READ);
   iic_read_byte(data, NACK);
   iic_stop();
   return *data;
}
