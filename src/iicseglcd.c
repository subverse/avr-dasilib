/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file      iicseglcd.c
///
///\brief      Driver for LCD segment driver PCF8577
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version      1.1 10/10/2010
///            - formatted for doxygen
///
///\version      1.0 10/08/2004
///
///   Copyright (C) 2005 David McKelvie
///
/////////////////////////////////////////////////////////
#include <avr/pgmspace.h>
#include <stdint.h>
#include "iic.h"
#include "pcf8574.h"
#include "iicseglcd.h"

//LCD segment data lookup table 0 1 2 3 4 5 6 7 8 9 . : -
static const char segment_table[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x80, 0x40};

//////////////////////////////////////////////////////////////////////
///
///\brief   Driver for Fuelquip (NZ) Ltd I2C PPU Display
///
///\details   An 8 bit parallel IO port (PCF8574) is used to set an
///         address line (A1) of an LCD segment driver (PCF8577).
///
///         The address of the PCF8574 is set using jumpers on the
///         PPU board. The board was designed to use address 1, 2,
///         and 3 using a single jumper, this can be extended to
///         include address 0 using two jumpers on both positions
///         1 and 2.
///
///         writing to the LCD segment driver is achieved by
///         first addressing the parallel IO port, using bit 7 of the
///         port to set the segment drivers address line, then writing
///         the address, command word and segment data to the segment
///         driver.
///
///         This method enables a single LCD segment driver address
///         to write to four different displays.
///
///         The display is a 4 digit 7 segment display, using auto
///         incrementing data loading, data is loaded LSB to MSB
///
///         The colon between digits 2 and 3 is mapped to the dot of
///         digit 4 (LSB)
///
///\param   address IIC_ADDRESS address of PCF8574 parallel IO port
///\param   data pointer to a four element (8 bit) array, each element
///         in the array is used as an index for the segmentData array.
///
///\return   1 on success
///
////////////////////////////////////////////////////////////////////////
uint8_t fq_ppu_display(uint8_t address, uint8_t *data)
{
   uint8_t i, buffer[4];

   for (i = 0; i < 4; i++)               //fill buffer with segment data
      if (data[3 - i] < 10)            //check range of data
         buffer[i] = segment_table[data[3 - i]];   //convert to segment data
      else
         buffer[i] = 0x40;               // out of range, place "-"

   if (address == IIC_ADDRESS_3)
      buffer[0] |= 0x80;               //Clock colon
   else
      buffer[3] |= 0x80;               //PPU dot

   iic_port_write(address, 0, 0);         //use bit 7 of PCF8574 to select PCF8577
   iic_write(PCF8577, 0x20, buffer, 4);      //write segment data to PCF8577
   return iic_port_write(address, 0, 0x80);   //de-select PCF8577
}

///////////////////////////////////////////////////////////////
///
///\brief   Display time on Fuelquip PPU display
///
///\return   1 on success
///
///////////////////////////////////////////////////////////////
uint8_t iic_show_time(void)
{
   uint8_t tmBuffer[2];
   uint8_t displayBuffer[4];

   if (iic_read(PCF8583, 3, tmBuffer, 2))
   {
      displayBuffer[0] = (tmBuffer[1] >> 4) & 0x03;      //10s of hours
      displayBuffer[1] = tmBuffer[1] & 0x0F;            //1s hours
      displayBuffer[2] = (tmBuffer[0] >> 4);
      displayBuffer[3] = tmBuffer[0] & 0x0F;
      fq_ppu_display(IIC_ADDRESS_3, displayBuffer);
      return 1;
   }
   return 0;
}

