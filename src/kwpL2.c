/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\file    kwpL2.c
///
///\brief   Implementation of layer 2 of ISO 14230 (KWP2000) and
///         VAG KWP1281
///
///\author    <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\verbatim
/// KWP1281
/// -------
/// KWP1281 differs from KWP2000 in that is uses a different header and
/// different data link protocol, i.e., all data bytes are inverted and
/// transmitted by any receiver until the special message end byte.
///
/// initialisation of the communication session is with the 5 baud init
/// method of KWP2000 except that the ECU does not reply to the inverted
/// KB2 of the tester with an inverted address but a block length which
/// is the start of an 'equipment data' message from the ECU.
///
/// KWP1281 message structure
/// |
/// | LEN | CNT | TYP |          DATA         | END |
///
/// LEN is the length of the message
/// CNT Message id is an incrementing count
/// TYP is the message type
///     0x05 - CLEAR error request
///     0x06 - session end
///     0x07 - GET error request
///     0x09 - ack
///     0x29 - group reading request
///     0xE7 - group reading reply
///     0xF6 - ASCII data
///     0xFC - GET error reply
/// END is the message end byte (0x03)
///
/// a receiver should invert and transmit all received bytes except
/// the END byte, to which the length of the next message or ack
/// should be transmitted.
///
/// Messages are acked
///
/// Typical communications start scenario
///
/// | TEST | ECU  | NOTES
/// |------|------|------
/// | 0x01 |      | tester initiates communications with a 5 baud sequence
/// |      | 0x55 | baud sync byte
/// |      | 0x01 | KB1
/// |      | 0x8A | KB2
/// | 0x75 |      | KB2 inverted
/// |      | 0x0F | LENGTH -----------------
/// | 0xF0 |      | LENGTH inverted         |
/// |      | 0x01 | message count           |--HEADER
/// | 0xFE |      | count inverted          |
/// |      | 0xF6 | message type ASCII      |
/// | 0xCF |      | ------------------------
/// ----------------------------------------
/// |      |      | VAG Part Number
/// ----------------------------------------
/// |      | 0x03 | END message
/// | 0x03 |      | LENGTH of ack ----------
/// |      | 0xFC |                         |
/// | 0x02 |      | message count           |--ACK
/// |      | 0xFD |                         |
/// | 0x09 |      | ACK                     |
/// |      | 0xF6 | ------------------------
/// | 0x03 |      | END
/// ----------------------------------------
/// |      |      | HEADER
/// ----------------------------------------
/// |      |      | VAG Component id
/// ----------------------------------------
/// |      |      | Tester ACK
/// ----------------------------------------
/// |      |      | HEADER
/// ----------------------------------------
/// |      |      | VAG Software Version
/// ----------------------------------------
/// |      |      | Tester ACK
/// ----------------------------------------
/// |      |      | HEADER
/// ----------------------------------------
/// |      |      | Workshop Code
/// ----------------------------------------
/// |      |      | Tester ACK
/// ----------------------------------------
/// |      |      | ECU ACK
/// ----------------------------------------
///
/// At the end of the above sequence the Tester is able to initiate the transfer of other
/// information or request action from the ECU.
///
///
/// KWP2000
/// |-------- HEADER -------|------DATA-------|-----|
/// | FMT | TGT | SRC | LEN | SID |   DATA    | CS  |
/// -------------------------                 -------
///
/// The underlined parts are implemented in this module, the DATA section
/// is implemented in kwpL3.c
///
/// TGT, SRC and LEN are optional depending upon FMT
///
/// --FORMAT--
/// The FMT byte contains 6 bit length information and 2 bit address format
///
/// |---------------FORMAT------------------|
/// | A1 | A0 | L5 | L4 | L3 | L2 | L1 | L0 |
///
/// | A1 | A0 |
/// | 1  | 1  | Functional Addressing
/// | 1  | 0  | Physical Addressing
/// | 0  | 1  | Exception Mode (CARB, ISO 9141-2)
/// | 0  | 0  | No Addressing
///
/// L5..L0 Length of DATA section above. If L5..L0 == 0 then the optional LEN
///        byte is used to give the message length
///\endverbatim
///
#include <avr/interrupt.h>
#include <string.h>
#include <stdint.h>
#include <util/delay.h>
#include "buffer.h"
#include "kwpL2.h"

// TCNT values for a prescaler of 1024, clk 16MHz
#define COUNT_5MS          0x004E
#define COUNT_100MS        0x061B
#define TCNT1_1MS          0xFFF0
#define TCNT1_2MS          0xFFE1
#define TCNT1_5MS          0xFFB1
#define TCNT1_10MS         0xFF63
#define TCNT1_20MS         0xFEC7
#define TCNT1_30MS         0xFE2B
#define TCNT1_40MS         0xFD8F
#define TCNT1_200MS        0xF3C9
#define TCNT1_300MS        0xEDAE
#define TCNT1_400MS        0xE793

#define KWP1281_BUFSIZ     10

static volatile kwp_tx_t tx;
static volatile kwp_rx_t rx;
static volatile timer_overflow_t timerOverflow;
static volatile input_capture_t inputCapture;

static volatile kwp_command_t command;
static volatile kwp_message_t message;

static volatile kwp1281_command_t kwp1281Command;
static volatile kwp1281_message_t kwp1281Message;

static volatile kwp_ecu_t ecu;

static uint8_t address = 0x35;

uint8_t kwp_get_ecu_state(void)
{
   return ecu.state;
}

uint8_t kwp_get_ecu_protocol(void)
{
   return ecu.protocol;
}

///\brief queue KWP command for later dispatch to ECU
uint8_t kwp_queue_command(uint8_t comm)
{
   if (ecu.state == ECU_OFFLINE)
      kwp_5b_init_device(ecu.address);

   if (!ecu.pending)
   {
      ecu.pending = 1;
      if (ecu.protocol == KWP1281)
      {
         kwp1281Command.header.type = comm;
      }
      else
      {
         command.sid = comm;
      }
      return 1;
   }
   else
      return 0;
}

///\brief request a measuring block from the ECU
uint8_t kwp_request_block(uint8_t comm)
{
   return kwp_queue_command(comm);
}

///\brief read a previously requested block
uint8_t kwp_read_block(uint8_t *data)
{
   return 0;
}

/*
 uint8_t kwp_message_ready( void )
 {
 if( rx.state == RX_GET_ECHO )
 return 1;
 return 0;
 }
 */

///\brief Get and return char from receiver buffer
///
uint8_t kwp_get_char(uint8_t *data)
{
   if (ecu.protocol == KWP1281)
      return buffer_get(data, &(kwp1281Message.data));
   else
      return buffer_get(data, &(message.data));
}

///\brief start communications with ecu
///\param device address of ecu
uint8_t kwp_start_communication(uint8_t device)
{
   return kwp_5b_init_device(device);
}

uint8_t kwp_init_device_fast(uint8_t device)
{
   K_LINE_ASSERT();
   _delay_ms(25);
   K_LINE_RELEASE();
   _delay_ms(25);
   return 1;
}

///\brief  ready sciTransmitter for transmission,
///        start transmission, enable interrupts
uint8_t kwp_start_tx(void)
{
   if (tx.state == TX_FINISHED || tx.state == TX_ERROR)
   {
      if (rx.state == RX_FINISHED || rx.state == RX_ERROR || rx.state == RX_GET_FMT)
      {
         tx.state = TX_SEND_FMT;    //reset transmitter state machine
         UCSRB |= _BV(TXEN) | _BV(UDRIE);  //enable transmitter, enable data register empty interrupts
         return 1;
      }
   }
   return 0;
}

///
///\brief initialise server at 5 baud ISO 14230 (5.2.4.2.2)
///
///\details Start the transmission of server address at 5 Baud 7-O-1
///      This begins the chain of events that, if successful, will result in:
///      -# The server address being written to the bus at 5 baud
///      -# The correct baud being set from the servers sync byte 0x55
///      -# The receiver being enabled and receiving the servers KB1 and KB2
///      The caller can then call:
///      - kwp_get_server_state() and test for "ECU_ONLINE"
///
///\param address address of server to initialise
///
uint8_t kwp_5b_init_device(uint8_t address)
{
   K_LINE_RELEASE();
   L_LINE_RELEASE();

   UCSRA = _BV(U2X);                               // set baud generator prescaler
   UCSRB = 0;                                      // disable transmitter and receiver
   UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);   // 8N1

   TIMSK &= ~( _BV(TOIE1) | _BV(TICIE1));         // disable timer 1 overflow and input capture interrupt
   TCCR1A = 0;                                     // normal timer 1 operation
   TCCR1B = _BV(CS12) | _BV(CS10);                 // timer 1 prescaler = clk/1024

   inputCapture.state = IC_IDLE_WAIT;             // init timer 1 input capture to detect bus activity

   timerOverflow.state = TOVF_SEND_START;         // Initialise timer 1 overflow interrupt...
   timerOverflow.data = address;

   TCNT1 = TCNT1_300MS;                           // Initialise timer 1 for overflow in 300ms
   TIFR |= _BV(TOV1) | _BV(ICF1);                // clear timer 1 overflow and input capture flags
   TIMSK |= _BV(TOIE1) | _BV(TICIE1);              // enable timer 1 overflow and input capture interrupt

   ecu.state = ECU_CONNECTING;
   ecu.protocol = PROTOCOL_UNKNOWN;

   return 1;
}

///
///\brief Timer 1 Overflow Interrupt
///
///\details Send server address byte at 5 baud on K and L lines,
/// then enable Input Capture interrupt to read sync byte
/// from server (0x55).
///
///\verbatim
/// |----|--300ms
///      |---|--200ms                            |----|--300ms
/// _____     ___                             _____________
///      |   |   |                           |
/// 0x01 | S | 0 | 1   2   3   4   5   6   P | S
///      |___|   |___|___|___|___|___|___|___|   |    |
///      t0  t1  t2  t3  t4  t5  t6  t7  t8  t9  t10  t11
///      ^   ^   ^   ^   ^   ^   ^   ^   ^   ^   ^    ^
///      |   |___|___|___|___|___|___|   |   |   |    |
///      |       |                       |   |   |    --TOVF_TIMEOUT, disable interrupts,
///      |       |                       |   |   |                   signal to main
///      |       |                       |   |   -------TOVF_TX_FINISHED, enable capture int,
///      |       |                       |   |                       enable timeout
///      |       |                       |   -----------TOVF_SEND_STOP
///      |       |                       ---------------TOVF_SEND_PARITY
///      |       ---------------------------------------TOVF_SEND_DATA
///      -----------------------------------------------TOVF_SEND_START
///\endverbatim
ISR(TIMER1_OVF_vect)
{
   static volatile uint8_t count = 0, parity = 0;

   TCNT1 = TCNT1_200MS;                      // reload counter for 200ms overflow
   switch (timerOverflow.state)
   {
      case TOVF_SEND_START:                   // falling edge of start bit
         timerOverflow.state = TOVF_SEND_DATA; // setup overflow interrupt to send address
         count = parity = 0;
         TIMSK &= ~_BV(TICIE1);                // disable input capture interrupt
         K_LINE_ASSERT();
         L_LINE_ASSERT();
         break;

      case TOVF_SEND_DATA:                    // send server address at 7-O-1
         if ((timerOverflow.data & 0x01) == 0x01)
         {
            parity++;                           // maintain count of '1s' for parity
            K_LINE_RELEASE();
            L_LINE_RELEASE();
         }
         else
         {
            K_LINE_ASSERT();
            L_LINE_ASSERT();
         }
         if (++count == 7)
            timerOverflow.state = TOVF_SEND_PARITY;
         else
            timerOverflow.data = timerOverflow.data >> 1;
         break;

      case TOVF_SEND_PARITY:                // send parity bit
         timerOverflow.state = TOVF_SEND_STOP;
         if ((parity & 0x01) == 0x01)
         {
            K_LINE_ASSERT();
            L_LINE_ASSERT();
         }
         else
         {
            K_LINE_RELEASE();
            L_LINE_RELEASE();
         }
         break;

      case TOVF_SEND_STOP:                    // start of stop bit
         timerOverflow.state = TOVF_TX_FINISHED;
         K_LINE_RELEASE();
         L_LINE_RELEASE();
         break;

      case TOVF_TX_FINISHED:                  // end of stop bit
         inputCapture.state = IC_BAUD_SET;     // setup input capture interrupt to measure baud
         timerOverflow.state = TOVF_TIMEOUT;   // setup timer 1 to timeout on no reply
         TCCR1B &= ~_BV(ICES1);                // negative edge causes capture
         TIFR |= _BV(ICF1);                  // clear input capture flag
         TIMSK |= _BV(TICIE1);                // enable input capture interrupt
         TCNT1 = TCNT1_300MS;                  // setup timer 1 for server timeout
         break;

      case TOVF_TIMEOUT:                      // to get here no server has responded
         ecu.state = ECU_OFFLINE;
         TIMSK &= ~_BV(TOIE1);                 // disable timer 1 overflow interrupt
         UCSRB &= ~( _BV(RXEN) | _BV(TXEN));  // disable transmitter and receiver
         break;

         // byte is loaded into UDR but transmitter is disabled to back off reply
      case TOVF_REPLY_DELAY:
         UCSRB |= _BV(TXCIE) | _BV(TXEN);      // enable transmitter and transmitter interrupt
         TIMSK &= ~_BV(TOIE1);                 // disable timer 1 overflow interrupt
         break;

      case TOVF_TX_ERROR:
      default:
         timerOverflow.state = TOVF_TX_ERROR;
         TIMSK &= ~_BV(TOIE1);                // disable timer overflow interrupt
   }
}

///
///\brief Timer 1 Input Capture Interrupt
///
///\details Capture edges of server sync byte (0x55), average values between
/// captures. Use result to set baud rate. Also used to detect bus activity
/// during idle times
///
///\verbatim
/// ______     ___     ___     ___     ___     _____
///       |   |   |   |   |   |   |   |   | P |
/// 0x55  | S | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | S  7-E-1 or 8-N-1
///       |___|   |___|   |___|   |___|   |___|
///       t0      t1      t2      t3      t4  t5
///       ^       ^       ^       ^       ^   ^
///       |       |       |       |       |   ----enable receiver, disable capture interrupts
///       |       |       |       |       --------take second capture, average difference, setup to
///       |       |       |       |                capture rising edge of stop bit
///       |       |       |       ----------------ignore
///       |       |       ------------------------take first capture
///       |       --------------------------------ignore
///       ----------------------------------------change prescaler to match baud rate generator
///
/// NOTES:   t0: when this interrupt occurs the timer 1 prescaler is 1024, this is because timer 1 is
///        also used as a timeout for a "wait for 0x55 from the server," the standard states that
///        this time can be up to 300ms. The timer prescaler is set to the same as the USART
///        baud rate generator (clk/8) at t0.
///
///      t1: ignored, this is because we want to have a capture that
///        we can shift right to average result instead of 16 bit division.
///
///      t2: this is the first capture with the timer prescaler setup correctly.
///
///      t3: ignored.
///
///      t4: this is the second capture, the difference between this and the first capture is
///        4 times the required bit time.
///
///      t5: this interrupt is used to enable the receiver and disable capture interrupts. Also
///        the prescaler is reset to 1024 so that the timer 1 overflow interrupt can
///        provide a reply delay.
///
/// Timer 1 is setup with the same prescaler as the Baud Rate Generator. So that
/// we can use the raw count between two edges as baud.
///
/// The timer counter can only rollover once for the allowable range of 1200 to 10400 baud.
///
/// Bit times and counts over the allowable range are:
///
///  BAUD   BIT TIME  TCNT at FOSC/8
///  1200   833 uS    1666  0x0682
///  9600   104 uS    208   0x00D0
///  10400  96 uS     192   0x00C0
///
/// Once the Baud rate is known the receiver is enabled to get keybytes from the server.
///\endverbatim
///
ISR(TIMER1_CAPT_vect)
{
   volatile uint16_t tmp = ICR1;
   static volatile uint16_t result = 0;
   static volatile uint8_t count = 0;

   switch (inputCapture.state)
   {
      case IC_BAUD_SET:
         inputCapture.state = IC_AUTO_BAUD;
         TIMSK &= ~_BV(TOIE1);            // disable timer 1 overflow interrupt
         TCCR1B = 0;                      // falling edge causes capture
         TCCR1B |= _BV(CS11);              // set prescaler to 8
         TIFR |= _BV(ICF1);              // clear input capture flag
         TIMSK |= _BV(TICIE1);            // enable input capture interrupt
         result = 0;
         count = 0;
         break;

      case IC_AUTO_BAUD:
         if (++count == 2)                // first timer sample
         {
            result = tmp;
         }
         else if (count == 4)             // second timer sample
         {
            if (tmp < result)              // deal with counter rollover
               result = (0xFFFF - result) + tmp;
            else
               result = tmp - result;

            result = (result >> 2) - 1;     // average result
            UBRRH = (uint8_t) (result >> 8) & 0x7F;
            UBRRL = (uint8_t) (result);    // baud and TCNT prescaler are same.
            TCCR1B |= _BV(ICES1);           // positive edge causes capture
            TIFR |= _BV(ICF1);            // clear input capture flag

         }
         else if (count == 5)
         {
            TIMSK &= ~_BV(TICIE1);            // disable input capture interrupt
            rx.state = RX_GET_KB1;            // setup receiver
            UCSRB |= _BV(RXEN) | _BV(RXCIE);  // enable receiver and receiver interrupts
            TCCR1B = _BV(CS12) | _BV(CS10);   // timer prescaler = clk/1024
            timerOverflow.state = TOVF_TIMEOUT;    // setup reply timeout
            TCNT1 = TCNT1_300MS;             // setup keybyte 1 timeout
            TIFR |= _BV(TOV1);               // clear timer overflow flag
            TIMSK |= _BV(TOIE1);              // enable timer overflow interrupt
         }
         break;

      case IC_IDLE_WAIT:                    // bus activity during idle wait
         TCNT1 = TCNT1_300MS;                // reset idle wait count
         break;

      default:
         inputCapture.state = IC_IDLE_WAIT;
   }
}

///
///\brief USART Receive Complete Interrupt
///
///\details
/// In KWP1281 Mode
/// -# receive server echo bytes and re-enable transmitter to send next data byte
/// -# receive server messages and enable transmitter to send echos
/// In normal mode
/// -# receive server messages
///
ISR(USART_RXC_vect)
{
   volatile uint8_t tmp, echo = 0;
   tmp = UDR;

   switch (rx.state)
   {
      // KWP1281
      case RX_GET_ECHO:                     // receive server echo
         UCSRB |= _BV(UDRIE);                // enable transmitter interrupt
         break;

      case RX_GET_KB1:                      // get and save Keybyte 1
         rx.state = RX_GET_KB2;
         UCSRB |= _BV(RXEN) | _BV(RXCIE);    // enable receiver
         ecu.kb1 = tmp;                      // save keybyte 1
         timerOverflow.state = TOVF_TIMEOUT; // setup keybyte 2 timeout
         TCNT1 = TCNT1_300MS;
         TIFR |= _BV(TOV1);                 // clear timer overflow flag
         TIMSK |= _BV(TOIE1);                // enable timer overflow interrupt
         break;

      case RX_GET_KB2:                      // get and save keybyte 2
         ecu.kb2 = tmp;
         timerOverflow.state = TOVF_REPLY_DELAY;
         TCNT1 = TCNT1_30MS;                // send echo of KB2 in 30ms (W4)
         TIFR |= _BV(TOV1);                 // clear timer overflow flag
         TIMSK |= _BV(TOIE1);                // enable timer overflow interrupt
         if ((ecu.kb1 == KWP1281_KB1) && (ecu.kb2 == KWP1281_KB2))
         {
            ecu.protocol = KWP1281;
            rx.state = RX_GET_LENGTH;
         }
         else
         {
            ecu.protocol = KWP2000;
            rx.state = RX_GET_ADDRESS;
         }
         ecu.state = ECU_ONLINE;             // signal to main
         buffer_init(&(kwp1281Message.data));
         break;

         // KWP1281
      case RX_GET_LENGTH:                   // get and save message length
         echo = 1;
         rx.state = RX_GET_COUNTER;
         kwp1281Message.header.length = tmp;
         break;

         // KWP1281
      case RX_GET_COUNTER:
         echo = 1;
         rx.state = RX_GET_TYPE;
         kwp1281Message.header.count = tmp;
         kwp1281Message.header.length--;
         break;

         // KWP1281
      case RX_GET_TYPE:
         echo = 1;
         kwp1281Message.header.type = tmp;
         kwp1281Message.header.length--;
         buffer_put(tmp, &(kwp1281Message.data));
         switch (tmp)
         // decode message type
         {
            case KWP1281_TYPE_ACK:
               rx.state = RX_GET_END;
               break;

            case KWP1281_TYPE_BLOCK_ACK:
               rx.state = RX_GET_BLOCK;
               break;

            case KWP1281_TYPE_ASCII:
               rx.state = RX_GET_ASCII;
               break;

            case KWP1281_TYPE_GET_ERROR_ACK:
               rx.state = RX_GET_ERROR;
               break;

            default:
               rx.state = RX_GET_ERROR;
               //TODO: handle protocol errors
         }
         break;

         // KWP1281
      case RX_GET_BLOCK:
         echo = 1;
         if (--kwp1281Message.header.length == 1)
         {
            rx.state = RX_GET_END;
         }
         if (!buffer_put(tmp, &(kwp1281Message.data)))
            ecu.state = BUFFER_FULL;
         break;

         // KWP1281
      case RX_GET_ASCII:                    // get character of ASCII message.
         echo = 1;
         if (--kwp1281Message.header.length == 1)
         {
            rx.state = RX_GET_END;
         }
         if (!buffer_put(tmp, &(kwp1281Message.data)))
            ecu.state = BUFFER_FULL;
         break;

         // KWP1281
      case RX_GET_ERROR:
         echo = 1;
         if (--kwp1281Message.header.length == 1)
            rx.state = RX_GET_END;
         if (!buffer_put(tmp, &(kwp1281Message.data)))
            ecu.state = BUFFER_FULL;
         break;

         // KWP1281
      case RX_GET_END:                      // EOM, 0x03
         UCSRB |= _BV(UDRIE);                // enable transmitter
         rx.state = RX_GET_ECHO;             // setup receiver to receive echos
         tx.state = TX_SEND_LENGTH;         // setup transmitter to send message
         kwp1281Command.header.length = 3;   // set length of ACK message
         kwp1281Command.header.type = KWP1281_TYPE_ACK;
         kwp1281Command.header.count = kwp1281Message.header.count + 1;

         if (kwp1281Message.header.type == KWP1281_TYPE_ACK)      // wait until server has no new data
            if (ecu.pending)          // check to see if group is requested
            {
               // TODO: setup transmitter to send command
               kwp1281Command.header.length = 3;
            }
         break;

      case PROTOCOL_ERROR:
         //TODO: Investigate protocol error recovery without another 5 baud init
         break;

      case RX_ERROR:
         break;

         // KWP2000
      case RX_GET_ADDRESS:
         // during 5 baud init the ecu sends its address inverted to the tester
         // once the address has been received by the tester the tester should
         // send the start communication request service ID
         command.header.format = 0x81;
         command.header.target = ecu.address;
         command.header.source = address;
         command.sid = KWP_SID_START_COMMUNICATIONS;
         command.header.checksum = 0;
         tx.state = TX_SEND_FMT;
         UCSRB |= _BV(TXCIE) | _BV(TXEN);      // enable transmitter and transmitter interrupt
         UCSRB &= ~(_BV(RXEN) | _BV(RXCIE));  // disable receiver
         break;

         // KWP2000
      case RX_GET_FMT:
         message.header.format = tmp;
         if (tmp & 0x3F)                      // format has length information
         {
            message.header.length = tmp & 0x3F;
            if (tmp & 0xC0)                    // header has addresses
               rx.state = RX_GET_TGT;            // get address next
            else
               // no length and no addresses
               rx.state = RX_GET_DATA;           // get data next
         }
         else                                  // header has length byte
         {
            if (tmp & 0xC0)                    // header has addresses
               rx.state = RX_GET_TGT;            // get address next
            else
               // length and no addresses
               rx.state = RX_GET_LEN;            // get length next
         }
         message.header.checksum = tmp;
         buffer_init(&(message.data));
         break;

         // KWP2000
      case RX_GET_TGT:
         // TODO: target address should be our address
         message.header.target = tmp;
         rx.state = RX_GET_SRC;
         message.header.checksum += tmp;
         break;

      case RX_GET_SRC:
         message.header.source = tmp;
         if (message.header.format & 0x3F)    // format has length information
            rx.state = RX_GET_DATA;
         else
            // header has length byte
            rx.state = RX_GET_LEN;
         message.header.checksum += tmp;
         break;

      case RX_GET_LEN:
         message.header.length = tmp;
         rx.state = RX_GET_DATA;
         message.header.checksum += tmp;
         break;

      case RX_GET_DATA:                       // length is data length excluding cs
         buffer_put(tmp, &message.data);
         if (--message.header.length == 0)
            rx.state = RX_GET_CS;
         message.header.checksum += tmp;
         break;

      case RX_GET_CS:
         if (message.header.checksum == tmp)
         {
            //TODO: message OK
         }
         else
         {
            //TODO: message error
         }
         rx.state = RX_GET_FMT;
         break;

      default:
         rx.state = RX_ERROR;
   }

   if (echo == 1)
   {
      UDR = ~tmp;                        // write inverted byte
      UCSRB |= _BV(TXCIE) | _BV(TXEN);      // enable transmitter and transmitter interrupt
      UCSRB &= ~(_BV(RXEN) | _BV(RXCIE));  // disable receiver
   }
}

///
///\brief USART Transmission Complete Interrupt
///
///\details
/// -# Disable transmitter after transmission of echo byte to ecu
/// -# Enable receiver to get next data byte from server
/// -# Setup timer for timeout interrupt
///
ISR(USART_TXC_vect)
{
   UCSRB &= ~(_BV(UDRIE) | _BV(TXCIE));   // disable transmitter interrupts
   UCSRB |= _BV(RXCIE) | _BV(RXEN);      // enable receiver and receiver interrupts
   timerOverflow.state = TOVF_TIMEOUT;     // setup reply timeout
   TCNT1 = TCNT1_300MS;
   TIFR |= _BV(TOV1);                     // clear timer overflow flag
   TIMSK |= _BV(TOIE1);                    // enable timer overflow interrupt
}

///
///\brief USART Transmitter Data Register Empty Interrupt
///\details
/// -# Send ACK to server after message reception
/// -# Send messages to server
///
ISR(USART_UDRE_vect)
{
   if (ecu.protocol == KWP1281)
   {
      UCSRB &= ~( _BV(UDRIE) | _BV(TXEN));   // disable UDRE interrupt and transmitter
      timerOverflow.state = TOVF_REPLY_DELAY; // setup timer to start transmission
      TCNT1 = TCNT1_2MS;                     // Initialise timer for overflow in 2ms
      TIFR |= _BV(TOV1);                     // clear timer overflow flag
      TIMSK |= _BV(TOIE1);                    // enable timer overflow interrupt
   }

   switch (tx.state)
   {
      // KWP1281
      case TX_SEND_LENGTH:
         //TCNT1  = TCNT1_10MS;                // Initialise timer 1 for overflow in 10ms
         tx.state = TX_SEND_COUNT;
         UDR = kwp1281Command.header.length;
         break;

         // KWP1281
      case TX_SEND_COUNT:
         tx.state = TX_SEND_TYPE;
         UDR = kwp1281Command.header.count;
         kwp1281Command.header.length--;
         break;

         // KWP1281
      case TX_SEND_TYPE:
         kwp1281Command.header.length--;
         UDR = kwp1281Command.header.type;
         if (kwp1281Command.header.type == KWP1281_TYPE_BLOCK_READ)
            tx.state = TX_SEND_PARAM;
         else
            tx.state = TX_SEND_END;
         break;

         // KWP1281
      case TX_SEND_PARAM:
         UDR = kwp1281Command.parameter;
         tx.state = TX_SEND_END;
         break;

         // KWP1281
      case TX_SEND_END:
         UDR = KWP1281_MESSAGE_END;
         tx.state = TX_FINISHED;
         rx.state = RX_GET_LENGTH;            // initialise receiver state machine
         break;

         // KWP2000
      case TX_SEND_FMT:                          // send Format byte
         if (command.header.length < 64)
            command.header.format |= command.header.length;
         UDR = command.header.format;
         command.header.checksum = 0;
         if (command.header.format && 0xC0)   // FMT has address information
         {
            tx.state = TX_SEND_TGT;
         }
         else
         {
            if (command.header.format && 0x3F) // FMT has length information
               tx.state = TX_SEND_DATA;
            else
               tx.state = TX_SEND_LEN;
         }
         command.header.checksum = command.header.format;
         break;

         // KWP2000
      case TX_SEND_TGT:                          // send Target Address
         UDR = command.header.target;
         tx.state = TX_SEND_SRC;
         command.header.checksum += command.header.target;
         break;

         // KWP2000
      case TX_SEND_SRC:                          // send Source Address
         UDR = command.header.source;
         if (command.header.format && 0x3F)  // Format byte contains length information
            tx.state = TX_SEND_DATA;
         else
            tx.state = TX_SEND_LEN;
         command.header.checksum += command.header.source;
         break;

         // KWP2000
      case TX_SEND_LEN:
         UDR = command.header.length;
         tx.state = TX_SEND_DATA;
         command.header.checksum += command.header.length;
         break;

         // KWP2000
      case TX_SEND_SID:
         UDR = command.sid;
         command.header.checksum += command.sid;
         if (command.header.length == 1)
            tx.state = TX_SEND_CHECKSUM;
         else
            tx.state = TX_SEND_DATA;
         command.header.checksum += command.sid;
         break;

         // KWP2000
      case TX_SEND_DATA:
         UDR = command.data[command.index];            //send command byte
         command.header.checksum += command.data[command.index];  //calculate checksum
         if (++command.index == command.header.length)              //test for end of message
            tx.state = TX_SEND_CHECKSUM;
         break;

         // KWP2000
      case TX_SEND_CHECKSUM:
         UDR = command.header.checksum;
         tx.state = TX_WAIT;
         break;

         // KWP2000
      case TX_WAIT:
         rx.state = RX_GET_FMT;            //initialise receiver state machine
         UCSRB &= ~(_BV(UDRIE) | _BV(TXEN));          //disable transmitter
         UCSRB |= _BV(RXEN) | _BV(RXCIE);            //enable receiver and receiver interrupts
         break;

      case TX_FINISHED:
      case TX_ERROR:
         UCSRB &= ~(_BV(UDRIE) | _BV(TXEN));  //disable transmitter, effective when reg. empty
         break;

      default:
         break;
   }
}
