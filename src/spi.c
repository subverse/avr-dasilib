/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
///\file   spi.c
///
///\brief   Driver for AVR SPI module
///
///\author   <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version   1.1 10/10/2010
///         - formatted for doxygen
///
///\version   1.0
///
///\verbatim
///PORT USAGE:
///
///      A0   X      B0   X      C0   X      D0   X
///      A1   X      B1   X      C1   X      D1   X
///      A2   X      B2   X      C2   X      D2   X
///      A3   X      B3   X      C3   X      D3   X
///      A4   X      B4   SS      C4   X      D4   X
///      A5   X      B5   MOSI   C5   X      D5   X
///      A6   X      B6   MUTE   C6   X      D6   X
///      A7   X      B7   SCK      C7   X      D7   X
///\endverbatim
///
#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include "spi.h"

#ifndef _NOP
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)
#endif

//////////////////////////////////////////////////////////////
///
///\brief Initialise SPI module
///
///////////////////////////////////////////////////////////////
uint8_t spi_init(void)
{
   DDRB |= _BV(PB4) | _BV(PB5) | _BV(PB7);      // MOSI, SCK and SS outputs.
   PORTB |= _BV(PB4) | _BV(PB5) | _BV(PB7);      // Enable pullups
   return 1;
}

//////////////////////////////////////////////////////////////
///
///\brief write character to SPI bus
///
///\param data data to write
///
///////////////////////////////////////////////////////////////
uint8_t spi_put_char(uint8_t data)
{
   SPI_PORT &= ~_BV(SS);
   SPDR = data;
   while (!( SPSR & _BV(SPIF)))
      ;         //wait for transmission
   SPI_PORT |= _BV(SS);
   return 1;
}

///////////////////////////////////////////////////////////////
///
///\brief write word to SPI bus
///
///\param data data to write
///
///////////////////////////////////////////////////////////////
uint8_t spi_put_word(uint16_t data)
{
   SPI_PORT &= ~_BV(SS);
   _NOP();
   _NOP();
   _NOP();
   SPDR = (uint8_t) (data >> 8);
   while (!( SPSR & _BV(SPIF)))
      ;         //wait for transmission
   SPDR = (uint8_t) data;
   while (!( SPSR & _BV(SPIF)))
      ;         //wait for transmission
   _NOP();
   _NOP();
   _NOP();
   SPI_PORT |= _BV(SS);
   _delay_us(10);
   return 1;
}
