/*
 * Copyright (C) 2016 David McKelvie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/////////////////////////////////////////////////////////
///
///\file      ipodsci.c
///
///\brief      UART driver for ipod dock project
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\details      This version implements a low layer interface
///            to an Apple iPod, using Apples Accessory Protocol.
///            This was developed using a 4G 40GB device.
///
///\version      1.0 05/05/2005
///
///   Copyright (C) 2005 David McKelvie
///
/////////////////////////////////////////////////////////
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include "buffer.h"
#include "ipodsci.h"

#define   UART_BAUD      19200UL
static ipod_receiver_t receiver;
ipod_transmitter_t transmitter;

///
///\brief Initialise USART
///
///\details   -Initialise USART for 19200 8N1.
///         -Initialise transmitter and receiver state machines.
///         -Does not enable TX or RX
///
uint8_t ipod_sci_init(void)
{
   cli();
   buffer_init(&(receiver.buffer));
   receiver.state = RX_FINISHED;

   transmitter.state = TX_FINISHED;
   transmitter.index = 0;
   transmitter.length = 0;
   transmitter.checksum = 0;

   //UBRRH = 0;                               //19200 baud
   //UBRRL = 25;                            //19200 baud
   //UCSRA = 0;
   UCSRB = 0;
   UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);    //8N1

#if F_CPU < 2000000UL
   UCSRA = _BV(U2X);
   UBRRL = (F_CPU / (8UL * UART_BAUD)) - 1;
#else
   UBRRL = (F_CPU / (16UL * UART_BAUD)) - 1;
#endif
   return 1;
}

///
///\brief Flush receiver buffer
///
uint8_t ipod_sci_flush_buffer(void)
{
   return buffer_init(&(receiver.buffer));
}

///
///\brief Start transmission
///
uint8_t ipod_sci_write(void)
{
   if (transmitter.state == TX_FINISHED || transmitter.state == TX_ERROR)
   {
      if (receiver.state == RX_FINISHED || receiver.state == RX_ERROR || receiver.state == WAITING_FOR_FF)
      {
         transmitter.state = SEND_FF;      //reset transmitter state machine
         UCSRB |= _BV(TXEN) | _BV(UDRIE);   //enable transmitter, enable data register empty interrupts
         sei();
         //enable global interrupts
         return 1;
      }
   }
   return 0;
}

///
///\brief Send single character
///
uint8_t ipod_sci_put_char(uint8_t data)
{
   while (!( UCSRA & _BV(UDRE)))
      ;
   UDR = data;
   return 1;
}

///
///\brief Read single character
///
uint8_t ipod_sci_get_char(uint8_t *data)
{
   if (receiver.state == RX_FINISHED)
      return buffer_get(data, &(receiver.buffer));
   return 0;
}

///
///\brief get a handle to the transmitter buffer
///
uint8_t *ipod_sct_get_tx_buffer(void)
{
   return transmitter.buffer;
}

///
///\brief USART receiver interrupt
///
///\details iPod message parsing state machine
///
ISR(USART_RXC_vect)
{
   uint8_t tmp;
   tmp = UDR;
   switch (receiver.state)
   {
      case WAITING_FOR_FF:
         if (tmp == 0xFF)
            receiver.state = WAITING_FOR_55;
         break;

      case WAITING_FOR_55:
         if (tmp == 0x55)
            receiver.state = WAITING_FOR_LENGTH;
         else
            receiver.state = RX_ERROR;
         break;

      case WAITING_FOR_LENGTH:
         receiver.checksum = tmp;
         receiver.length = tmp;
         receiver.tmplen = tmp;
         receiver.state = GETTING_MESSAGE;
         break;

      case GETTING_MESSAGE:
         receiver.checksum += tmp;
         if (!buffer_put(tmp, &(receiver.buffer)))
            receiver.state = RX_ERROR;
         if (!--receiver.tmplen)
            receiver.state = WAITING_FOR_CHECKSUM;
         break;

      case WAITING_FOR_CHECKSUM:
         //TODO: put checksum check here
         UCSRB &= ~_BV(RXEN);   //disable receiver
         receiver.state = RX_FINISHED;
         break;

      case RX_FINISHED:
      case RX_ERROR:
         UCSRB &= ~_BV(RXEN);   //disable receiver
         break;

      default:
         receiver.state = RX_ERROR;
   }
}

///
///\brief USART transmitter interrupt
///
ISR(USART_UDRE_vect)
{
   switch (transmitter.state)
   {
      case SEND_FF:                                          //send first header byte
         if (2 < transmitter.length && transmitter.length < 11)      //sanity check command length
         {
            transmitter.state = SEND_55;
            transmitter.index = 0;
            UDR = 0xFF;
         }
         else
            transmitter.state = TX_ERROR;
         break;

      case SEND_55:                                          //send second header byte
         transmitter.state = SEND_LENGTH;
         UDR = 0x55;
         break;

      case SEND_LENGTH:
         transmitter.checksum = transmitter.length;               //start checksum calc.
         transmitter.state = SEND_MESSAGE;
         UDR = transmitter.length;
         break;

      case SEND_MESSAGE:
         transmitter.checksum += transmitter.buffer[transmitter.index];      //calculate checksum
         UDR = transmitter.buffer[transmitter.index++];                  //send command byte
         if (transmitter.index == transmitter.length)                     //test for end of message
            transmitter.state = SEND_CHECKSUM;
         break;

      case SEND_CHECKSUM:
         transmitter.checksum = (transmitter.checksum ^ 0xff) + 1;
         transmitter.state = TX_FINISHED;
         UDR = transmitter.checksum;
         PORTA = transmitter.checksum;
         break;

      case TX_FINISHED:
         UCSRB &= ~( _BV(UDRIE) | _BV(TXEN));               //disable transmitter
         receiver.state = WAITING_FOR_FF;                  //initialise receiver state machine
         UCSRB |= _BV(RXEN) | _BV(RXCIE);                  //enable receiver and receiver interrupts
         break;

      case TX_ERROR:
         UCSRB &= ~( _BV(UDRIE) | _BV(TXEN));   //disable transmitter, effective when reg. empty
         break;

      default:
         transmitter.state = TX_ERROR;
   }
}
